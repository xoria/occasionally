# ROADMAP

## Misc High Prio

- Fix: `rnd_verify_token` postgres function off-by-one
- Fix: create peer with initial form groups assigned is not assigned those
       groups
- Chore: Split modules by use-case (e.g. create_revent.rs), not 
         model/service/repository

## Invites (EMail)

- Feat: Send mails; select templates from DB
- Feat: Send invite mails (with entry in DB)

## Personal use

- Security: Disable Register via environment flag (BE->UI via GET /app/info)
- Chore: Define CI Pipeline
- Chore: Setup demo instance and auto-deploy

## Closed Alpha (MVP)

- Feat: Manage account text_templates
- Chore: Write README

## Basic features

- Feat: Markdown support
- Feat: Mobile UI layout frame
- Chore: Remove `clientRnd` factor (was a silly idea) and add links to Emails
- Security: Re-add password based login
- Security: Password reset

## Public Service

- Feat: Author-specific SMTP config (highly limit mails per hour/day/month, if
        not configured)
- Chore: Check legal requirements (cookies, imprint, contract)
- Feat: Peer opt-out from further invites / mails (deny-list)
- Security: Registration / Login CAPTCHA protection + honeypot
- Security: IP- & Account-based rate limits (use memcached)
- Security: Request sanitization (request body size)
- Chore: Build release archives

## Buddy Invites

- Feat: Add buddy invite form
- Feat: Use previously invited buddies for quick-invite (typeahead field)

## Transports

- Feat: Telegram Transport
- Feat: Matrix Transport
- Feat: Signal Transport

## Comments / Chat

- Feat: Comments on Agenda Items / Event
- Feat: Hidden comments on invites (for author to read)

## Admin Interface

- TBD

# BACKLOG

- Chore: Create overview of not-yet-implemented db tables / columns
- Feat: Indicate unsaved changes (maybe even add confirm dialog to site
        navigation)
- Perf: Use internal serial bigint ID (foreign keys, server lookups, etc.) in
        addition to public ID11 for improved db indices
- Feat: Dark Mode support
- Fix: `@vueuse: useUrlSearchParams` write option not working as expected
- Feat: Send custom notification mails, actively triggered by author
- Feat: Public revents list
- Feat: Provide ICS / DAV for peers & accounts
- Feat: Import CSV / VCF Peers
- Feat: Export CSV / VCF Peers

## Very Low Prio

- Chore: Serve fonts
- Feat: WhatsApp Transport
- Chore: Rename DB fields `date_*` to `*_at`
- Chore: Use naming convention for relationship tables
- Chore: Avoid i32 for IDs, use i64 instead
- Perf: Database Indices (e.g. token tables)
- Chore: Cronjob for database row expiry
- Perf: Use memcached for session states, db only on cache miss
- UX/SEO/Perf: Server-side (pre-)rendering strategy
- Perf: Benchmark regex vs onig vs tinytemplate crates for template util
- Perf: Paginate list queries
