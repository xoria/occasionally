# Revently

## Status Quo

This is very early stage. It's not ready for even prototypical setup yet. I just
believe that software does not have to be ready in order to be open sourced.

Feel free to add a star, if you want to state your interest in this project.

## Planned early Features

* Suited for **events of any size**, may it be small private meetups or huge
  conferences:
  * Optional agenda features allow for huge events to still be organized.
  * For mid-sized events, the agenda items can be created by your peers
    themselves.
  * Invite-only, access by link or public events.
* **Participation estimation**:
  * State your deadlines to decide if the event takes place at all.
  * See the interest in the event and its agenda items for a perfect allocation
    of resources.
* Event-related **chat**:
  * Threaded discussions for each event and even individual agenda items.
* **Markdown** syntax for content fragments:
  * Templates can be saved for re-use (e.g. address information, etc.).
* Data protection
  * Archive events shortly after their timeline; keep only event definition and
    metadata (e.g. number of participants). Remove chat and even participant
    names by default (cannot be enabled on SaaS plan).
* Provide a service for public registration

## Additional Features (low priority)

* ical Attachment & URL
  * Peer-based URL to contain all their invitations & accept/reject state
  * Attachment for emails & messenger(?)
* Call for participation with scheduling system
* Multi-dimensional agenda display (e.g. split to multiple rooms)
* Additional transport methods (matrix/signal/telegram bot) in addition to email
* Custom Avatars
* Assets to attach to / show within event / agenda item

## Code Architecture to be

* Frontend: Typescript; probably going to be Svelte or Vue3
* Backend: I go for rust with actix and deadpool-postgres. This will be my first serious rust project. Let's see how it goes... Fallback: node.js with typescript
* DB: PostgreSQL, with some custom extension (via rust)

## License & Contribution

This software is licensed under terms of AGPL-3.0, unless otherwise specified.
See LICENSE.txt for details.

Contributions are welcome. Please try to keep the code clean.
