See https://github.com/tcdi/pgx/blob/master/cargo-pgx/README.md on how to use.

Quick hints:

- Run in a local postgresql v14 database: `cargo pgx run pg14` (use `cargo pgx connect` to re-connect without re-build)
- Install globally: `cargo pgx package`, then merge target/release/pg_rnd_numeric-pg*/ into your global file tree. Afterwards run `CREATE EXTENSION pg_rnd_numeric;` within your psql shell.

