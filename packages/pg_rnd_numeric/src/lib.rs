use pgx::prelude::*;

pgx::pg_module_magic!();

#[pg_extern]
fn rnd_int8() -> i64 {
    rand::random::<i64>()
}

#[pg_extern]
fn rnd_int4() -> i32 {
    rand::random::<i32>()
}

#[pg_extern]
fn rnd_int2() -> i16 {
    rand::random::<i16>()
}

#[pg_extern]
fn rnd_real() -> f32 {
    rand::random::<f32>()
}

#[pg_extern]
fn rnd_double() -> f64 {
    rand::random::<f64>()
}

#[pg_extern]
fn rnd_char() -> i8 {
    rand::random::<i8>()
}

#[pg_extern]
fn rnd_bytea(size: i32) -> Vec<u8> {
    (0..size).map(|_| { rand::random::<u8>() }).collect()
}
