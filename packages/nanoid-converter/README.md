# nanoid-converter

A simple nodejs tool to convert hex codes from/to short URL-safe IDs.

## Motivation

I did want to use short IDs (similar to youtube video IDs for example) in URLs.
I could not find any postgres extension that could store an `int8` with a short
ID input/output interface. I might write such a type some day...

For now, I found it easier to just convert the ID within the nodejs web-server,
so for all that postgres knows, I use random `int8` IDs that I convert to hex
for input/output.
