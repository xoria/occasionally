import { idFromHex, idToHex } from "./conversion";

export interface ConverterInternals {
  alphabet: string;
  inverse: { [key: string]: number; };
  alphabetSize: number;
  hexSize: number;
  idSize: number;
  lastHexBits: number;
  bpc: number;
}

export interface NanoIDConverter {
  idToHex: (id: string) => string;
  idFromHex: (hex: string) => string;
}

export function createConverter(characters: string, idSize: number): NanoIDConverter {
  console.assert(characters.length > 0);
  console.assert(idSize > 0);

  let bpc = 1;
  let maxSize = 2;
  while (characters.length > maxSize) {
    bpc++;
    maxSize *= 2;
  }
  const hexSize = ((idSize * bpc + 3) / 4) | 0; // equivalent to Math.ceil(idSize * bpc / 4)

  const inverse: ConverterInternals["inverse"] = {};
  for (let i = 0; i < characters.length; i++) {
    inverse[characters[i]] = i;
  }

  const parsedAlphabet: ConverterInternals = {
    alphabet: characters,
    inverse: inverse,
    alphabetSize: characters.length,
    hexSize,
    idSize,
    lastHexBits: (idSize * bpc) & 3 || 4,
    bpc
  };

  return {
    idToHex: idToHex.bind(null, parsedAlphabet),
    idFromHex: idFromHex.bind(null, parsedAlphabet),
  }
}
