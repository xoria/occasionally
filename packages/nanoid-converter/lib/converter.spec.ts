import { createConverter } from "./converter";

describe("converter", () => {
  it("should convert IDs that take up full bytes", () => {
    const converter = createConverter("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-", 2);
    // Af => 0110.1000.0101 => 685
    expect(converter.idToHex("Af")).toBe("685");
    expect(converter.idFromHex("685")).toBe("Af");
  });

  it("should convert with partial hex remainder", () => {
    const converter = createConverter("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-", 3);
    // Af_ => 0110.1000.0101.1111.10 => 685F2
    expect(converter.idToHex("Af_")).toBe("685F2");
    expect(converter.idFromHex("685F2")).toBe("Af_");
  });

  it("should work with small alphabets", () => {
    const converter = createConverter("abc", 6);
    // abccbb => 0001.1010.0101 => 1a5
    expect(converter.idToHex("abccbb")).toBe("1A5");
    expect(converter.idFromHex("1A5")).toBe("abccbb");
  });

  it("should left-pad hex input", () => {
    const converter = createConverter("abcd", 4);
    expect(converter.idFromHex("3")).toBe("aaad");
    expect(converter.idFromHex("")).toBe("aaaa");
    expect(converter.idFromHex("0")).toBe("aaaa");
  });

  it("should accept lower-case hex input", () => {
    const converter = createConverter("abcd", 6);
    expect(converter.idFromHex("1a5")).toBe("abccbb");
    expect(converter.idFromHex("1Af")).toBe("abccdd");
  });
});
