import type { ConverterInternals } from "./converter";

const HEX = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"]
const HEX_INV = {
  "0": 0,
  "1": 1,
  "2": 2,
  "3": 3,
  "4": 4,
  "5": 5,
  "6": 6,
  "7": 7,
  "8": 8,
  "9": 9,
  "a": 10,
  "b": 11,
  "c": 12,
  "d": 13,
  "e": 14,
  "f": 15,
  "A": 10,
  "B": 11,
  "C": 12,
  "D": 13,
  "E": 14,
  "F": 15,
}

export function idToHex({
                          bpc,
                          inverse,
                          idSize
                        }: ConverterInternals, id: string): string {
  console.assert(id.length === idSize, "Invalid ID size");

  let hex = "";
  let value = 0;
  let bits = 0;

  for (let i = 0; i < id.length; i++) {
    value <<= bpc;
    value += inverse[id[i]];
    bits += bpc;
    while (bits > 4) {
      let offsetBits = bits - 4;
      hex += HEX[value >> offsetBits];
      value &= (1 << offsetBits) - 1;
      bits -= 4;
    }
  }

  if (bits > 0) {
    hex += HEX[value];
  }

  return hex;
}

export function idFromHex({
                            bpc,
                            alphabet,
                            hexSize,
                            lastHexBits
                          }: ConverterInternals, hex: string): string {
  let _len = hex.length;
  console.assert(_len <= hexSize, "Invalid hex data (too long)");

  let id = "";
  let value = 0;
  let bits = 0;

  for (; hexSize > _len; hexSize--) {
    bits += 4;
    while (bits > bpc) {
      id += alphabet[0];
      bits -= bpc;
    }
  }

  for (let i = 0; i < _len; i++) {
    const _bits = i === _len - 1 ? lastHexBits : 4;
    value <<= _bits;
    bits += _bits;
    value += HEX_INV[hex[i]];
    while (bits > bpc) {
      let offsetBits = bits - bpc;
      id += alphabet[value >> offsetBits];
      value &= (1 << offsetBits) - 1;
      bits -= bpc;
    }
  }

  if (bits > 0) {
    id += alphabet[value];
  }

  return id;
}
