const tailwindTheme = require("tailwindcss/defaultTheme");
const tailwindColors = require("tailwindcss/colors");
const plugin = require("tailwindcss/plugin");
const Color = require("color");
const { times } = require("lodash");

const buttonsPlugin = require("./scripts/tailwind-buttons-plugin.cjs");

const colors = {
  transparent: tailwindColors.transparent,
  current: tailwindColors.current,
  fill: "fillColor",

  white: tailwindColors.white,
  black: tailwindColors.black,

  pale: tailwindColors.slate,
  primary: tailwindColors.sky,
  secondary: tailwindColors.fuchsia,
  tertiary: tailwindColors.amber,

  error: tailwindColors.red,
  warn: tailwindColors.amber,
  success: tailwindColors.lime,
  info: tailwindColors.stone,
};

colors.translucent = {
  dark: Object.fromEntries(
    times(9, (i) => [
      (i + 1) * 100,
      new Color(colors.pale["900"])
        .alpha((i + 1) * 0.1)
        .rgb()
        .string(),
    ])
  ),
  light: Object.fromEntries(
    times(9, (i) => [
      (i + 1) * 100,
      new Color(colors.pale["100"])
        .alpha((i + 1) * 0.1)
        .rgb()
        .string(),
    ])
  ),
};

/** @type {import("tailwindcss").Config} */
module.exports = {
  prefix: "tw-",
  content: ["./index.html", "./src/**/*.{ts,vue}"],
  theme: {
    fontFamily: {
      main: ["inter", ...tailwindTheme.fontFamily.sans],
      heading: ["Lato", "latolatin", ...tailwindTheme.fontFamily.sans],
      mono: [
        "Source Code Pro",
        "Noto Sans Mono",
        "Roboto Mono",
        ...tailwindTheme.fontFamily.mono,
      ],
    },
    colors,
    container: {
      center: true,
      padding: {
        DEFAULT: tailwindTheme.spacing["2"],
        lg: tailwindTheme.spacing["4"],
      },
    },
    zIndex: {
      auto: tailwindTheme.zIndex.auto,
      overlay: 50,
    },
    extend: {
      borderColor: {
        DEFAULT: colors.pale["300"],
      },
      outlineColor: {
        DEFAULT: colors.primary["500"],
      },
      spacing: {
        "2px": "2px",
        128: "32rem",
        144: "36rem",
      },
    },
  },
  plugins: [
    "@headlessui/tailwindcss",
    "@tailwindcss/typography",
    require("@tailwindcss/forms")({
      strategy: "class", // only generate classes
    }),
    "@tailwindcss/line-clamp",
    "@tailwindcss/aspect-ratio",

    plugin(function ({ addVariant }) {
      addVariant("hocus", ["&:hover", "&:focus"]);
    }),

    buttonsPlugin,
  ],
};
