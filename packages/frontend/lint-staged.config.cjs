module.exports = {
  "*.{ts,js,cjs,vue,html,pcss}": "prettier --write --",
  "*.{ts,js,cjs,vue}": "eslint --fix --",
  "*.{pcss,vue}": "stylelint --fix --",
};
