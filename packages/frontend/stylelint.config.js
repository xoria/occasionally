module.exports = {
  extends: [
    "stylelint-config-standard",
    "stylelint-config-rational-order",
    "stylelint-config-recommended-vue",
    "stylelint-config-prettier",
  ],
  plugins: ["stylelint-prettier"],
  rules: {
    "at-rule-no-unknown": [
      true,
      {
        ignoreAtRules: [
          "tailwind",
          "apply",
          "variants",
          "responsive",
          "screen",
          "layer",
        ],
      },
    ],
    "order/properties-alphabetical-order": null,
    "no-descending-specificity": null,
    "selector-class-pattern": null,
    "function-no-unknown": null,
    "max-nesting-depth": 4,
  },
};
