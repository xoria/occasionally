/// <reference types="vitest" />
import { fileURLToPath, URL } from "node:url";

import { defineConfig } from "vite";
// @ts-ignore
import eslint from "vite-plugin-eslint";
// @ts-ignore
import stylelint from "vite-plugin-stylelint";
// @ts-ignore
import svgLoader from "vite-svg-loader";
import vue from "@vitejs/plugin-vue";
import Components from "unplugin-vue-components/vite";
// @ts-ignore
import VueIconsResolver from "@kalimahapps/vue-icons/resolver";
import * as path from "path";

const cacheLocation = path.resolve(__dirname, "target/build-cache");

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    eslint({
      cache: true,
      cacheLocation: path.join(cacheLocation, "lint.eslint"),
    }),
    stylelint({
      emitErrorAsWarning: true,
      cacheLocation: path.join(cacheLocation, "lint.stylelint"),
    }),
    svgLoader(),

    vue(),

    Components({ resolvers: [VueIconsResolver] }),
  ],
  envPrefix: "REVENTLY_",
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
  },
  css: {
    postcss: path.resolve(__dirname, "postcss.config.js"),
  },
  server: {
    proxy: {
      "/api": {
        target: process.env.API_URL ?? "http://localhost:8080",
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
  test: {
    environment: "happy-dom",
    root: "src/",
    setupFiles: ["config/test.setup.ts"],
    restoreMocks: true,
    deps: {
      inline: ["element-plus"],
    },
  },
});
