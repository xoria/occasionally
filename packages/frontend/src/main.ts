import "./styles/global/index.pcss";

import app from "@/app";

import "./config/vee-validate.config";

app.mount("#app");
