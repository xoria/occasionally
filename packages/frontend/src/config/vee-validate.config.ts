import { defineRule } from "vee-validate";
import { FORM_ERROR_CODE } from "@/common/locale/locale.types";

const emailValidator = createEmailValidator();

defineRule(
  "required",
  (value: unknown): true | FORM_ERROR_CODE =>
    isSome(value) || FORM_ERROR_CODE.REQUIRED
);

defineRule(
  "email",
  (value: string): true | FORM_ERROR_CODE =>
    (isSome(value) && emailValidator(value)) || FORM_ERROR_CODE.EMAIL
);

function isSome(value: unknown) {
  return (
    value != null && value !== "" && (!Array.isArray(value) || value.length > 0)
  );
}

function createEmailValidator() {
  const el = document.createElement("input");
  el.type = "email";

  return (value: string) => {
    el.value = value;
    const isValid = el.checkValidity();
    el.value = "";
    return isValid;
  };
}
