import type { DateDto, Dto } from "@/util/dto";
import { castDto, dateFromDto, dateToDto } from "@/util/dto";
import type { MarkOptional } from "ts-essentials";
import { omit, pick } from "lodash-es";
import type { MarkNullable } from "@/util/types";

export enum ReventVisibility {
  PUBLIC = "Public",
  INVITE_ONLY = "InviteOnly",
  LINK = "Link",
}

export interface ContentFragment {
  id: number;
  source: string;
}
export interface ContentFragmentDto extends ContentFragment, Dto {}

interface ReventAgendaBase {
  interestMin: null | number;
  interestMax: null | number;
}

interface ReventAgenda extends ReventAgendaBase {
  dateBegin: Date;
  dateEnd: null | Date;
}

interface ReventAgendaDto extends ReventAgendaBase, Dto {
  dateBegin: DateDto;
  dateEnd: null | DateDto;
}

function reventAgendaFromDto(dto: ReventAgendaDto): ReventAgenda {
  return {
    ...dto,
    dateBegin: dateFromDto(dto.dateBegin),
    dateEnd: dateFromDto(dto.dateEnd),
  };
}

interface ReventBase {
  id: string;
  name: string;
  visibility: ReventVisibility;
}

export interface Revent extends ReventBase, ReventAgenda {
  dateCreated: Date;
  dateArchive: null | Date;
}
export interface ReventDto extends ReventBase, ReventAgendaDto {
  dateCreated: DateDto;
  dateArchive: null | DateDto;
}

export interface ReventFull extends Revent {
  content: ContentFragment[];
}

export interface ReventFullDto extends ReventDto {
  content: ContentFragmentDto[];
}

export function reventFromDto(dto: ReventDto): Revent {
  return {
    ...dto,
    ...reventAgendaFromDto(dto),
    dateCreated: dateFromDto(dto.dateCreated),
    dateArchive: dateFromDto(dto.dateArchive),
  };
}
export function reventFullFromDto(dto: ReventFullDto): ReventFull {
  return {
    ...reventFromDto(dto),
    content: dto.content.map(castDto),
  };
}

export function reventsFromDto(dto: ReventDto[]): Revent[] {
  return dto.map(reventFromDto);
}

interface RootAgendaUpsertBase {
  interestMin: ReventAgendaBase["interestMin"];
  interestMax: ReventAgendaBase["interestMax"];
}
interface RootAgendaUpsert extends RootAgendaUpsertBase {
  dateBegin: ReventAgenda["dateBegin"];
  dateEnd: ReventAgenda["dateEnd"];
}
interface RootAgendaUpsertDto extends RootAgendaUpsertBase, Dto {
  dateBegin: ReventAgendaDto["dateBegin"];
  dateEnd: ReventAgendaDto["dateEnd"];
}

function rootAgendaUpsertToDto(it: RootAgendaUpsert): RootAgendaUpsertDto {
  return castDto({
    ...it,
    dateBegin: dateToDto(it.dateBegin),
    dateEnd: dateToDto(it.dateEnd),
  });
}

export interface ReventUpsertBase extends RootAgendaUpsertBase {
  id?: Revent["id"];
  name: Revent["name"];
  visibility: Revent["visibility"];
}
export interface ReventUpsertContentItem
  extends MarkOptional<ContentFragment, "id"> {
  modified: boolean;
  _key: number | symbol;
}
export interface ReventUpsert extends ReventUpsertBase, RootAgendaUpsert {
  dateArchive: Revent["dateArchive"];
  content: ReventUpsertContentItem[];
}
type ReventUpsertContentItemDto =
  | MarkOptional<ContentFragmentDto, "id">
  | ContentFragmentDto["id"];
export interface ReventUpsertDto
  extends Dto,
    ReventUpsertBase,
    RootAgendaUpsertDto {
  dateArchive: ReventDto["dateArchive"];
  content: ReventUpsertContentItemDto[];
}

export type ReventUpsertModel = MarkNullable<ReventUpsert, "dateBegin">;

export function reventUpsertToDto(it: ReventUpsert): ReventUpsertDto {
  return castDto({
    ...it,
    ...rootAgendaUpsertToDto(it),
    dateArchive: dateToDto(it.dateArchive),
    content: it.content.map(reventUpsertContentItemToDto),
  });
}

function reventUpsertContentItemToDto(
  it: ReventUpsertContentItem
): ReventUpsertContentItemDto {
  if (it.modified || it.id === undefined) {
    return castDto(omit(it, "modified", "_key"));
  }
  return it.id;
}

function reventAgendaToRootUpsert(it: ReventAgenda): RootAgendaUpsert {
  return pick(it, "interestMin", "interestMax", "dateBegin", "dateEnd");
}

export function reventFullToUpsert(it: ReventFull): ReventUpsert {
  return {
    ...pick(it, "id", "name", "visibility", "dateArchive"),
    ...reventAgendaToRootUpsert(it),
    content: it.content.map(contentFragmentToUpsert),
  };
}

function contentFragmentToUpsert(it: ContentFragment): ReventUpsertContentItem {
  return { ...it, modified: false, _key: it.id };
}
