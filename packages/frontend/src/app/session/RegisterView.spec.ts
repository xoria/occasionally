import { describe, it, expect } from "vitest";

import RegisterView from "./RegisterView.vue";
import { vueRender } from "../../util/test/vue-render";

describe("RegisterView", () => {
  it("renders properly", () => {
    const view = vueRender(RegisterView);
    expect(view.html()).toMatchSnapshot();
  });
});
