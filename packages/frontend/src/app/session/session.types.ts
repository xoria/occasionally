import type { Dto } from "@/util/dto";
import { identity } from "lodash-es";
import { castDto } from "@/util/dto";

export enum SignInOrUpInitFormType {
  REGISTER = "REGISTER",
  LOGIN = "LOGIN",
}

export interface SignInOrUpInit {
  email: string;
}
export type SignInOrUpInitDto = SignInOrUpInit & Dto;

export const signInOrUpInitToDto = castDto as (
  it: SignInOrUpInit
) => SignInOrUpInitDto;

export interface SignInOrUpInitResult {
  clientRnd: string;
}
export type SignInOrUpInitResultDto = SignInOrUpInitResult & Dto;

export const signInOrUpInitResultFromDto = identity as (
  dto: SignInOrUpInitResultDto
) => SignInOrUpInitResult;

export interface SignInOrUpVerifyModel {
  code: string;
}
export interface SignInOrUpVerify extends SignInOrUpVerifyModel {
  clientRnd: string;
}
export type SignInOrUpVerifyDto = SignInOrUpVerify & Dto;

export const signInOrUpVerifyToDto = castDto as (
  it: SignInOrUpVerify
) => SignInOrUpVerifyDto;
