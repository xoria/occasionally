import type { Dto } from "@/util/dto";
import { identity } from "lodash-es";
import { castDto } from "@/util/dto";
import type { EventBusKey } from "@vueuse/core";

export interface PeerGroup {
  id: number;
  name: string;
}
export type PeerGroupDto = PeerGroup & Dto;

export const peerGroupFromDto = identity as (dto: PeerGroupDto) => PeerGroup;
export const peerGroupsFromDto = identity as (
  dto: PeerGroupDto[]
) => PeerGroup[];

export interface PeerBase {
  id: number;
  name: string;
  email: string; // TODO remove nullability fom db
  buddyPeerId: number | null;
  avatarSeed: string;
  displayName: string; // TODO make nullable
}
export interface Peer extends PeerBase {
  peerGroups: Set<PeerGroup["id"]>;
}
export interface PeerDto extends PeerBase, Dto {
  peerGroups: PeerGroup["id"][];
}

export function peerFromDto(dto: PeerDto): Peer {
  return {
    ...dto,
    peerGroups: new Set(dto.peerGroups),
  };
}
export const peersFromDto = (list: PeerDto[]) => list.map(peerFromDto);

interface PeerUpsertBase {
  id?: PeerBase["id"];
  name: PeerBase["name"];
  email: PeerBase["email"];
}

export interface PeerUpsert extends PeerUpsertBase {
  peerGroups: Peer["peerGroups"];
}
export interface PeerUpsertDto extends PeerUpsertBase, Dto {
  peerGroups: PeerDto["peerGroups"];
}

export function peerUpsertToDto(it: PeerUpsert): PeerUpsertDto {
  return castDto({
    ...it,
    peerGroups: [...it.peerGroups],
  });
}

export interface PeerGroupCreate {
  name: PeerGroup["name"];
}
export type PeerGroupCreateDto = PeerGroupCreate & Dto;

export const peerGroupCreateToDto = castDto as (
  it: PeerGroupCreate
) => PeerGroupCreateDto;

export interface PeerUpdatedEvent {
  newPeer: Peer;
  oldPeer: Peer;
}

export const PEER_GROUP_CREATED_SYM: EventBusKey<PeerGroup> =
  Symbol("PeerGroup:Created");
