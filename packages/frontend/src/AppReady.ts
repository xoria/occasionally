import { defineComponent, h } from "vue";
import { useSession } from "@/common/session/session.store";
import { useLocale } from "@/common/locale/locale.store";
import AppReadyFailed from "@/AppReadyFailed.vue";

export default defineComponent({
  name: "AppReady",
  setup: async (_, ctx) => {
    const session = useSession();
    const locale = useLocale();

    try {
      await Promise.all([session.ready, locale.ready]);
    } catch (err) {
      console.error("App init failed due to:", err);
      return () => h(AppReadyFailed);
    }

    return ctx.slots.default;
  },
});
