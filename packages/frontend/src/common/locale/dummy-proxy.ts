interface DummyAccessGet {
  key: string | symbol;
}

interface DummyAccessCall {
  called: true;
  arguments: unknown[];
}

type DummyAccess = DummyAccessCall | DummyAccessGet;

interface DummyData {
  (): DummyData;
  path: DummyAccess[];
  escape?: any;
}

export function createDummyProxy(
  name = "DUMMY",
  escape: any = {
    __v_raw: undefined,
    __v_isRef: false,
    __v_isReadonly: true,
    name: "",
  }
) {
  return mkDummyProxy(name, [], escape) as unknown;
}

function mkDummyProxy(
  name: string,
  path: DummyAccess[],
  escape?: any
): DummyData {
  const data = function (...args: unknown[]): DummyData {
    return mkDummyProxy(
      name,
      [...path, { called: true, arguments: args }],
      escape
    );
  } as DummyData;
  data.path = path;

  return new Proxy<DummyData>(data, {
    get(_, key: string | symbol): any {
      if (key === "toJSON") {
        return () => path;
      }
      if (Reflect.has(escape, key)) {
        return escape[key];
      }
      if (key === "toString" || key === Symbol.toPrimitive) {
        return () => `{ ${name}/${pathToString(path)} }`;
      }
      return mkDummyProxy(name, [...path, { key }], escape);
    },
    apply(_, __, argArray: any[]): any {
      return mkDummyProxy(
        name,
        [...path, { called: true, arguments: argArray }],
        escape
      );
    },
  });
}

function pathToString(path: DummyAccess[]): string {
  const parts = path.reduce<string[]>((parts, current) => {
    if (isAccessCall(current)) {
      const callPart = `(${current.arguments
        .map((it) => JSON.stringify(it))
        .join(",")})`;
      if (parts.length > 0) {
        parts[parts.length - 1] += callPart;
      } else {
        parts.push(callPart);
      }
    } else {
      parts.push(current.key.toString());
    }
    return parts;
  }, []);
  return parts.join(".");
}

function isAccessCall(access: DummyAccess): access is DummyAccessCall {
  return Reflect.has(access, "called");
}
