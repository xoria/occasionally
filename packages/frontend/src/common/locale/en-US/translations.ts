import type { FORM_ERROR_CODE } from "@/common/locale/locale.types";
import type { SignInOrUpInitFormType } from "@/app/session/session.types";
import type { SimpleKey } from "@/util/types";

const formError: { [Key in FORM_ERROR_CODE]: string } = {
  REQUIRED: "Required",
  EMAIL: "Email is not valid",
};

const signInOrUpAction: Record<SignInOrUpInitFormType, string> = {
  REGISTER: "Register",
  LOGIN: "Login",
};

const genericError = {
  title: "Error",
  anything: "Oops. Some error occurred. Please try again.",
};

const common = {
  loading: "Loading...",
  noResults: "Nothing here yet",
  createdAt: "Created at",
  range: (a: SimpleKey, b: SimpleKey) => `${a} to ${b}`,
};

export default {
  layout: {
    sourceCodeAt: "Source Code at ",
  },

  action: {
    navBack: "Back",
    next: "Next",
    close: "Close",
    verify: "Verify",
    cancel: "Cancel",
    save: "Save",
    edit: "Edit",
    create: "Create",
    createIt: (item: string) => `Create new ${item}`,
    delete: "Delete",

    ...signInOrUpAction,
  },

  form: {
    error: formError,

    verifyCode: "Verification Code",
    email: "Email",
    name: "Name",
  },

  common,

  genericError,

  revent: {
    title: "Event",
    plural: "Events",
    notFoundInfo: "Start creating your first event with the button above.",
    deletedNotify: {
      title: "Event deleted successfully.",
    },
    numberOfParticipants: "Number of participants",
    participants: (min: SimpleKey, max: SimpleKey) =>
      `${common.range(min, max)} participants`,
    beginAt: "Starts at",
  },

  peer: {
    title: "Peer",
    plural: "Peers",
    notFoundInfo: "Start adding new peers with the button above.",
  },

  peerGroup: {
    title: "Peer Group",
  },

  session: {
    displayName: {
      guest: "<guest>",
      anonymous: "<anonymous>",
    },

    register: "Register",
    login: "Login",
    verifyCode: {
      title: "Verify via Email Code",
      body: "Please enter the verification code that has been sent to your email address below.",
    },
  },
};
