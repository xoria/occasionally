import translations from "./translations";
import type { Locale } from "../locale.pure-types";

const localeEnUS: Locale = {
  name: "en-US",
  translations,
};

export default localeEnUS;
