import { ref } from "vue";
import type { Ref } from "vue";
import { createGlobalState } from "@vueuse/core";

import type { Locale, LocaleName, Translations } from "./locale.pure-types";
import { createDummyProxy } from "@/common/locale/dummy-proxy";

const DEFAULT_LOCALE: LocaleName = "en-US";

export interface LocaleStoreValue {
  name: Ref<LocaleName | null>;
  t: Ref<Translations>;

  ready: Promise<void>;
  setLocale(locale: LocaleName): Promise<void>;
}

export const useLocale = createGlobalState<LocaleStoreValue>(() => {
  const ready = setLocale(DEFAULT_LOCALE);
  const data: LocaleStoreValue = {
    name: ref(null),
    t: ref(createDummyProxy("TRANSLATE") as Translations),

    ready,
    setLocale,
  };

  async function setLocale(name: LocaleName): Promise<void> {
    const locale = await fetchLocale(name);
    data.name.value = locale.name;
    data.t.value = locale.translations;
    console.debug("locales loaded", name, locale);
  }

  return data;
});

async function fetchLocale(locale: LocaleName): Promise<Locale> {
  return (await import(`./${locale}/index.ts`)).default as Locale;
}
