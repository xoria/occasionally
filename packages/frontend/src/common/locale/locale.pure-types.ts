import type translations from "./en-US/translations";

export type Translations = typeof translations;

export type LocaleName = "en-US";

export interface Locale {
  name: LocaleName;
  translations: Translations;
}
