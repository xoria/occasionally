export enum HttpStatusCode {
  OK_NO_CONTENT = 204,

  ERR_NOT_FOUND = 404,
}
