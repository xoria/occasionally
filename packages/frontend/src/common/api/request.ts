import { useAsync, useAsyncFactory } from "@/util/async";
import type { PromiseWithCancel } from "@/util/cancel";
import { identity } from "lodash-es";
import { HttpStatusCode } from "@/common/api/response.types";
import { reactiveNaiveTyped } from "@/util/vue";
import { useErrorHandling } from "@/util/error";
import type { Dto } from "@/util/dto";

const API_URL = import.meta.env.REVENTLY_API_URL;

type CustomRequestInit<Body> = Omit<RequestInit, "body"> &
  (Body extends void ? unknown : { body: Body });

export function apiRequest(
  _url: string,
  init: RequestInit = {}
): PromiseWithCancel<Response> {
  return fetchWithCancel(new Request(`${API_URL}${_url}`, init));
}

function _useRequestFactory<Result, Body = void>(
  url: string | (() => string),
  init: RequestInit = {},
  serializeBody: (it: Body) => unknown,
  deserializeResponse: (it: Response) => Promise<Result> | Result = identity
) {
  const { handleServerError } = useErrorHandling();

  return useAsyncFactory<
    Result,
    Body extends void
      ? [Omit<RequestInit, "body">] | []
      : [CustomRequestInit<Body>]
  >((localInit = {} as CustomRequestInit<Body>) => {
    const _url = typeof url === "function" ? url() : url;
    const body = (localInit as any).body as Body;
    const { promise, ...rest } = apiRequest(_url, {
      ...init,
      ...localInit,
      body:
        body !== undefined ? JSON.stringify(serializeBody(body)) : undefined,
      headers: {
        "Content-Type": "application/json",
        ...(init.headers ?? {}),
        ...(localInit.headers ?? {}),
      },
    });
    return {
      promise: promise.then(deserializeResponse).catch(tapServerError),
      ...rest,
    };
  });

  function tapServerError(err: any): never {
    if (err instanceof Response || err instanceof DOMException) {
      handleServerError(err);
    }
    throw err;
  }
}

export function useRequestFactory<Body = void>(
  url: string | (() => string),
  init?: RequestInit
) {
  return _useRequestFactory<Response, Body>(url, init, identity, pluckOk);
}

export function useJSONRequestFactory<
  BodyDto extends void | Dto | Dto[],
  ResultDto extends void | Dto | Dto[]
>(url: string | (() => string), init?: RequestInit) {
  return <Body = BodyDto, Result = ResultDto>(
    resultFromDto: (it: ResultDto) => Result = identity,
    bodyToDto: (it: Body) => BodyDto = identity
  ) =>
    _useRequestFactory<Result, Body>(url, init, bodyToDto, (res) =>
      pluckBodyJSON<ResultDto, Result>(res, resultFromDto ?? identity)
    );
}

function _useRequest<Result, Body = void>(
  url: string,
  init: CustomRequestInit<Body> = {} as CustomRequestInit<Body>,
  serializeBody: (it: Body) => unknown,
  deserializeResponse: (it: Response) => Promise<Result> | Result = identity
) {
  const { handleServerError } = useErrorHandling();

  const body = (init as unknown as { body: Body }).body;
  const { promise, ...rest } = apiRequest(url, {
    ...init,
    body: body !== undefined ? JSON.stringify(serializeBody(body)) : undefined,
    headers: {
      "Content-Type": "application/json",
      ...(init.headers ?? {}),
    },
  });
  return useAsync<Result>({
    promise: promise.then(deserializeResponse).catch(tapServerError),
    ...rest,
  });

  function tapServerError(err: any): never {
    if (err instanceof Response || err instanceof DOMException) {
      handleServerError(err);
    }
    throw err;
  }
}

export function useRequest<Body = void>(
  url: string,
  init?: CustomRequestInit<Body>
) {
  return _useRequest<Response, Body>(url, init, identity, pluckOk).state;
}

export function useJSONRequest<
  BodyDto extends void | Dto | Dto[],
  ResultDto extends void | Dto | Dto[]
>(url: string) {
  return <Body = BodyDto & Dto, Result = ResultDto>(
    init?: CustomRequestInit<Body>,
    resultFromDto: (it: ResultDto) => Result = identity,
    bodyToDto: (it: Body) => BodyDto = identity
  ) =>
    _useRequest<Result, Body>(url, init, bodyToDto, (res) =>
      pluckBodyJSON<ResultDto, Result>(res, resultFromDto)
    ).state;
}

function fetchWithCancel(req: Request): PromiseWithCancel<Response> {
  const controller = new AbortController();
  return {
    promise: fetch(req, { signal: controller.signal }),
    cancel: () => controller.abort(),
  };
}

async function pluckOk(res: Response): Promise<Response> {
  if (res.ok) {
    return res;
  }
  throw res;
}

async function pluckBodyJSON<T, U>(
  res: Response,
  transform: (it: T) => U
): Promise<U> {
  if (res.ok) {
    if (res.status === HttpStatusCode.OK_NO_CONTENT) {
      return undefined as unknown as U;
    }
    return reactiveNaiveTyped(transform((await res.json()) as T));
  }
  throw res;
}
