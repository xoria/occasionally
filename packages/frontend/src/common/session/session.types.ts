import type { Dto } from "@/util/dto";
import { identity } from "lodash-es";

export interface AccountSelf {
  email: string;
  username: string | null;
}
export type AccountSelfDto = AccountSelf & Dto;

export const accountSelfFromDto = identity as (
  dto: AccountSelfDto
) => AccountSelf;

export interface AccountForeign {
  email: undefined;
  username: string | null;
}
export type AccountForeignDto = AccountForeign & Dto;

export const accountForeignFromDto = identity as (
  dto: AccountForeignDto
) => AccountForeign;

export type Account = AccountSelf | AccountForeign;
export type AccountDto = AccountSelfDto | AccountForeignDto;

export const accountFromDto = identity as (dto: AccountDto) => Account;
