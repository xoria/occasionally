import { createGlobalState } from "@vueuse/core";
import type { Ref } from "vue";
import { ref } from "vue";
import { useJSONRequestFactory } from "@/common/api/request";
import type {
  AccountSelf,
  AccountSelfDto,
} from "@/common/session/session.types";
import { accountSelfFromDto } from "@/common/session/session.types";

export interface SessionStoreValue {
  account: Ref<AccountSelf | null>;
  ready: Promise<void>;

  setState(account: AccountSelf | null): void;

  fetchState(): Promise<void>;
}

export const useSession = createGlobalState<SessionStoreValue>(() => {
  const req = useJSONRequestFactory<never, AccountSelfDto | undefined>(
    "/session/self"
  )((it) => it && accountSelfFromDto(it));
  const ready = fetchState();

  const data: SessionStoreValue = {
    account: ref(null),
    ready,
    setState,
    fetchState,
  };

  function setState(account: AccountSelf | null) {
    data.account.value = account;
  }

  async function fetchState(): Promise<void> {
    setState((await req.trigger()) ?? null);
  }

  return data;
});

export async function useSessionReady() {
  const session = useSession();
  await session.ready;
  return session;
}
