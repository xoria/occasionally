import { vi } from "vitest";
import type { SessionStoreValue } from "@/common/session/session.store";
import { ref } from "vue";

const mockSessionStore: SessionStoreValue = {
  setState: vi.fn(),
  fetchState: vi.fn(),
  account: ref(null),
  ready: Promise.resolve(),
};

vi.mock("../session.store.ts", async () => {
  const actual = await vi.importActual("../session.store.ts");

  return {
    ...(actual as any),

    useSession: () => mockSessionStore,
  };
});
