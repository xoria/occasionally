import type { Account } from "@/common/session/session.types";
import { computed } from "vue";
import type { ComputedRef, WatchSource } from "vue";
import { useLocale } from "@/common/locale/locale.store";
import { unwrapWatchSource } from "@/util/vue";

export function useDisplayName(
  _account: WatchSource<Account | null>
): ComputedRef<string> {
  const { t } = useLocale();

  return computed(() => {
    const account = unwrapWatchSource(_account);
    if (account === null) {
      return t.value.session.displayName.guest;
    }
    const { username, email } = account;
    return username ?? email ?? t.value.session.displayName.anonymous;
  });
}
