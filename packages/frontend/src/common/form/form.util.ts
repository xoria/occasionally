import type { Ref } from "vue";
import { computedEager } from "@vueuse/core";
import type { MaybeRef } from "@vueuse/core";
import { useLocale } from "@/common/locale/locale.store";
import type { FORM_ERROR_CODE } from "@/common/locale/locale.types";
import type { FieldContext, FieldMeta, FieldOptions } from "vee-validate";
import { useField } from "vee-validate";
import type { Translations } from "@/common/locale/locale.pure-types";
import { computed, isRef, ref } from "vue";

export interface FormFieldContext<TValue = unknown>
  extends Omit<FieldContext<TValue>, "meta"> {
  errorMessage: Ref<string | undefined>;
  label?: Ref<string | undefined>;
  meta: Ref<FieldMeta<TValue> & { required: boolean }>;
}

export interface FormFieldOptions<TValue = unknown>
  extends Omit<FieldOptions<TValue>, "label"> {
  label?: (t: Translations) => string;
}

export function useFormField<FormModel, Prop extends keyof FormModel>(
  name: MaybeRef<Prop>,
  rules?: MaybeRef<Record<string, unknown>>,
  opts?: Partial<FormFieldOptions<FormModel[Prop]>>
): FormFieldContext<FormModel[Prop]> {
  const { t } = useLocale();
  const field = useField(name as MaybeRef<string>, rules, {
    ...opts,
    label: opts?.label ? computed(() => opts.label!(t.value)) : undefined,
  });
  const required = isRef(rules)
    ? computedEager(() => !!rules.value.required)
    : ref(!!rules?.required);

  const meta = computed(() => {
    return { ...field.meta, required: required.value };
  });

  return {
    ...field,
    errorMessage: useFormErrorMessage(field),
    label: field.label as undefined | Ref<string | undefined>,
    meta,
    handleBlur: (evt?: Event) => {
      field.handleBlur(evt);
      field.validate().catch(console.error);
    },
  };
}

function useFormErrorMessage(field: FieldContext): Ref<string | undefined> {
  const { t } = useLocale();
  return computedEager(() => {
    const { value } = field.errorMessage;
    if (value === undefined) {
      return value;
    }
    return t.value.form.error[value as FORM_ERROR_CODE];
  });
}
