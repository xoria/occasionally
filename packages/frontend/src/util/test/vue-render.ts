import { render } from "@testing-library/vue";
import type { RenderOptions } from "@testing-library/vue";

export function vueRender(component: any, options: RenderOptions = {}) {
  return render(component, {
    ...options,

    global: {
      plugins: [],

      ...(options.global ?? {}),

      config: {
        warnHandler,
        ...(options.global?.config ?? {}),
      },

      provide: {
        // [ID_INJECTION_KEY as symbol]: { prefix: 0, current: 0 },
        ...(options.global?.provide ?? {}),
      },
    },
  });
}

function warnHandler(...args: unknown[]) {
  // filter warnings that are caused by the TRANSLATE DummyProxy and vue prop validation
  if (typeof args[0] === "string") {
    if (
      /^Invalid prop: type check failed for prop "[^"]+". Expected String with value "\{ TRANSLATE\/[^}]* }", got Function\s*$/.test(
        args[0]
      )
    ) {
      return;
    }
  }
  return console.error(...args);
}
