import { onBeforeUnmount, shallowReactive } from "vue";
import type { ShallowReactive } from "vue";
import type { PromiseWithCancel } from "@/util/cancel";
import { noop, once } from "lodash-es";
import { isCancellable } from "@/util/cancel";

interface AsyncStateInitial {
  loading: false;
  ready: false;
  result: undefined;
}

type AsyncStateReset = AsyncStateInitial;

interface AsyncStateLoadingInitial {
  loading: true;
  ready: false;
  result: undefined;
}

interface AsyncStateLoadingReady<T> {
  loading: true;
  ready: true;
  result: T;
}

type AsyncStateLoading<T> =
  | AsyncStateLoadingInitial
  | AsyncStateLoadingReady<T>;

interface AsyncStateReady<T> {
  loading: false;
  ready: true;
  result: T;
}

export type AsyncFactoryState<T> =
  | AsyncStateInitial
  | AsyncStateLoading<T>
  | AsyncStateReady<T>
  | AsyncStateReset;

export type AsyncState<T> =
  | AsyncStateLoadingInitial
  | AsyncStateReady<T>
  | AsyncStateReset;

export interface UseAsyncFactory<Result, Args extends unknown[]> {
  state: ShallowReactive<AsyncFactoryState<Result>>;
  trigger: (...args: Args) => Promise<Result>;
  cancel: (opts?: { reset?: boolean }) => void;
}

export function useAsyncFactory<Result, Args extends unknown[]>(
  fn: (...args: Args) => Promise<Result> | PromiseWithCancel<Result>
): UseAsyncFactory<Result, Args> {
  const state = shallowReactive<AsyncFactoryState<Result>>({
    loading: false,
    ready: false,
    result: undefined,
  });

  let runId = -1;
  let readyId = -1;
  let cancelFn: () => void = noop;

  onBeforeUnmount(cancelFn);

  return { state, trigger, cancel };

  async function trigger(...args: Args): Promise<Result> {
    const _runId = ++runId;
    state.loading = true;
    const res = fn(...args);
    const _isCancellable = isCancellable(res);

    cancelFn = _isCancellable ? once(res.cancel) : noop;
    try {
      const result = await (_isCancellable
        ? res.promise
        : (res as Promise<Result>));
      if (_runId > readyId) {
        state.ready = true;
        state.result = result;
        readyId = _runId;
      }
      if (_runId === runId) {
        state.loading = false;
        cancelFn = noop;
      }
      return result;
    } catch (err) {
      state.loading = false;
      state.ready = false;
      state.result = undefined;
      throw err;
    }
  }

  function cancel({ reset }: { reset?: boolean } = {}) {
    if (runId > readyId) {
      cancelFn();
    }
    if (reset) {
      readyId = runId;
      state.loading = false;
      state.ready = false;
      state.result = undefined;
    }
  }
}

export interface UseAsync<Result> {
  state: ShallowReactive<AsyncState<Result>>;
  cancel: () => void;
}

export function useAsync<Result>(
  it: Promise<Result> | PromiseWithCancel<Result>
): UseAsync<Result> {
  const state = shallowReactive<AsyncState<Result>>({
    loading: true,
    ready: false,
    result: undefined,
  });

  (isCancellable(it) ? it.promise : (it as Promise<Result>))
    .then((result) => {
      const _state = state;
      if (state.loading) {
        _state.ready = true;
        _state.result = result;
        _state.loading = false;
      }
    })
    .catch((err) => {
      throw err;
    });

  onBeforeUnmount(() => cancel());

  return { state, cancel };

  function cancel({ reset }: { reset?: boolean } = {}) {
    if (state.loading && isCancellable(it)) {
      it.cancel();
    }
    if (reset) {
      state.loading = false;
      state.ready = false;
      state.result = undefined;
    }
  }
}
