export interface ListWithLookup<T> {
  list: T[];
  lookup: Record<string, T>;
}

export function withLookup<T extends { id: string | number }>(
  list: T[]
): ListWithLookup<T>;
export function withLookup<T>(
  list: T[],
  keyFn: (it: T) => string
): ListWithLookup<T>;
export function withLookup<T>(
  list: T[],
  keyFn: (it: T) => string = (it) => (it as any).id.toString()
): ListWithLookup<T> {
  const lookup: Record<string, T> = {};
  for (const item of list) {
    lookup[keyFn(item)] = item;
  }
  return { list, lookup };
}
