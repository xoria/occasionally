import type { Ref, WatchSource } from "vue";
import { isRef, reactive, ref, watch } from "vue";
import type { MaybeRef } from "@vueuse/core";

export function unwrapWatchSource<T>(source: WatchSource<T>): T {
  if (isRef(source)) {
    return source.value;
  }
  return source();
}

export function unwrapMaybeRef<T>(source: MaybeRef<T>): T {
  if (isRef(source)) {
    return source.value;
  }
  return source;
}

export function computedWrite<T>(source: WatchSource<T>): Ref<T> {
  const localState = ref<T>() as Ref<T>;
  watch(source, (value) => (localState.value = value), { immediate: true });
  return localState;
}

export const reactiveNaiveTyped = reactive as <T>(it: T) => T;
