/**
 * Never used at runtime, just to ensure proper type casts.
 */
const __OPAQUE_TYPE__ = Symbol();

export interface Dto {
  readonly [__OPAQUE_TYPE__]: "DTO";
}

// todo unfortunately the default serde/deserialize does not support unix
//  timestamps but only ISO strings; thus we use them for now... We have to use
//  the default deserializer as it's not possible yet to compose custom
//  (de)serializers with `Option` or similar wrapper structs
// export type DateDto = number;
export type DateDto = string;

export function castDto<T>(it: T): T & Dto {
  return it as T & Dto;
}

export function dateFromDto(it: DateDto): Date;
export function dateFromDto(it: DateDto | null): Date | null;
export function dateFromDto(it: DateDto | null): Date | null {
  // return it === null ? null : new Date(it * 1000);
  return it === null ? null : new Date(it);
}

export function dateToDto(it: Date): string;
export function dateToDto(it: Date | null): DateDto | null;
export function dateToDto(it: Date | null): DateDto | null {
  // return it === null ? null : (it.getTime() / 1000) | 0;
  return it === null ? null : it.toISOString();
}
