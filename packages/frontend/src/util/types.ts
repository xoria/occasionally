export type SimpleKey = string | number;

export type MarkNullable<T, K extends keyof T> = T extends T
  ? Omit<T, K> & { [Key in K]: T[Key] | null }
  : never;
