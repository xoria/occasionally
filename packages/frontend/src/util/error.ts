import { useNotification } from "@kyvg/vue3-notification";
import { useLocale } from "@/common/locale/locale.store";

export function useErrorHandling() {
  const { t } = useLocale();
  const { notify } = useNotification();

  return {
    handleServerError(res: Response | DOMException) {
      if (res instanceof DOMException) {
        // cancelled request; do nothing
        console.debug("Cancelled request", res);
        return;
      }

      console.error("Server error", res);
      notify({
        type: "error",
        group: "reaction",
        title: t.value.genericError.title,
        text: t.value.genericError.anything,
        duration: 4000,
      });
    },
  };
}
