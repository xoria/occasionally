import type { ShallowRef, WatchSource } from "vue";
import { shallowRef, watch } from "vue";
import { times } from "lodash-es";

export function useCountTimes<T>(
  fields: WatchSource<number>,
  iteratee: (index: number) => T
): ShallowRef<T[]> {
  const result = shallowRef<T[]>([]);

  watch(
    fields,
    (fields) => {
      const prevValue = result.value;
      if (prevValue.length === fields) {
        return;
      }
      if (prevValue.length > fields) {
        result.value = prevValue.slice(0, fields);
      } else {
        const idxBase = prevValue.length;
        result.value = [
          ...prevValue,
          ...times(fields - prevValue.length, (i) => iteratee(idxBase + i)),
        ];
      }
    },
    { immediate: true }
  );

  return result;
}
