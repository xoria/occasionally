export interface PromiseWithCancel<T> {
  promise: Promise<T>;
  cancel: () => void;
}

export function isCancellable<T>(
  it: Promise<T> | PromiseWithCancel<T>
): it is PromiseWithCancel<T> {
  return !(it instanceof Promise);
}
