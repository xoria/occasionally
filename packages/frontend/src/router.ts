import { createRouter, createWebHistory } from "vue-router";
import { useSession, useSessionReady } from "@/common/session/session.store";
import { apiRequest } from "@/common/api/request";
import { useErrorHandling } from "@/util/error";
import VoidComponent from "@/util/VoidComponent";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      beforeEnter: async () => {
        const session = await useSessionReady();
        if (session.account.value !== null) {
          return { name: "home--user" };
        }
      },
      component: () => import("@/app/landing/LandingView.vue"),
    },

    {
      path: "/",
      name: "session:guest",
      beforeEnter: async () => {
        const session = await useSessionReady();
        if (session.account.value !== null) {
          return { name: "home--user" };
        }
      },
      children: [
        {
          path: "/login",
          name: "login",
          component: () => import("@/app/session/LoginView.vue"),
        },
        {
          path: "/register",
          name: "register",
          component: () => import("@/app/session/RegisterView.vue"),
        },
      ],
    },

    {
      path: "/",
      name: "session:user",
      beforeEnter: async () => {
        const session = await useSessionReady();
        if (session.account.value === null) {
          return { name: "login" };
        }
      },
      children: [
        {
          path: "/welcome",
          name: "home--user",
          component: () => import("@/app/landing/LandingView.vue"),
        },
        {
          path: "/revents",
          name: "revents",
          component: () => import("@/app/revent/ReventsView.vue"),
        },
        {
          path: "/revent/new",
          name: "revent:create",
          component: () => import("@/app/revent/ReventEditView.vue"),
        },
        {
          path: "/revent/:id/edit",
          name: "revent:edit",
          component: () => import("@/app/revent/ReventEditView.vue"),
        },
        {
          path: "/revent/:id/edit/content",
          name: "revent:edit-content",
          component: () => import("@/app/revent/ReventEditContentView.vue"),
        },
        {
          path: "/peers",
          name: "peers",
          component: () => import("@/app/peer/PeersView.vue"),
        },
        {
          path: "/logout",
          name: "logout",
          beforeEnter: async () => {
            const session = useSession();
            const { handleServerError } = useErrorHandling();
            const res = await apiRequest("/session/logout", { method: "POST" })
              .promise;

            if (!res.ok) {
              handleServerError(res);
              return false;
            }
            session.setState(null);
            return { name: "home" };
          },
          component: VoidComponent,
        },
      ],
    },
  ],
});

export default router;
