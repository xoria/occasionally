import { vitest } from "vitest";

vitest.mock("vue-router", () => ({
  useRoute: vitest.fn(),
  useRouter: vitest.fn(() => ({
    push: () => {},
    replace: () => {},
  })),
}));
