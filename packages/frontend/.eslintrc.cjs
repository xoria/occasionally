/* eslint-env node */
require("@rushstack/eslint-patch/modern-module-resolution");

module.exports = {
  root: true,
  extends: [
    "plugin:vue/vue3-essential",
    "eslint:recommended",
    "@vue/eslint-config-typescript",
    "@vue/eslint-config-prettier",
    "plugin:tailwindcss/recommended",
  ],
  plugins: ["vue", "tailwindcss"],
  rules: {
    "tailwindcss/no-custom-classname": ["off"],
  },
  ignorePatterns: ["auto-imports.d.ts", "components.d.ts"],
  overrides: [
    {
      files: ["cypress/e2e/**/*.{cy,spec}.{js,ts,jsx,tsx}"],
      extends: ["plugin:cypress/recommended"],
    },
    {
      files: ["src/**/?(*.)+(spec|test).[jt]s?(x)"],
      extends: ["plugin:testing-library/vue"],
    },
    {
      files: ["*.config.js", "*.cjs"],
      env: {
        commonjs: true,
      },
    },
  ],
  parserOptions: {
    ecmaVersion: "latest",
  },
};
