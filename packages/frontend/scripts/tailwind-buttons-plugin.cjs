const plugin = require("tailwindcss/plugin");

module.exports = plugin(function ({ addComponents, config, theme }) {
  const prefix = config("prefix");
  const spacing = theme("spacing");

  const padding = {
    sm: { x: spacing["2"], y: spacing["0.5"] },
    _: { x: spacing["4"], y: spacing["2"] },
    lg: { x: spacing["5"], y: spacing["3"] },
    xl: { x: spacing["8"], y: spacing["6"] },
  };

  const components = Object.fromEntries([
    ...Object.keys(theme("colors")).flatMap((color) => {
      const colorDef = theme(`colors.${color}`);
      if (typeof colorDef !== "object" || !Reflect.has(colorDef, "500")) {
        return [];
      }

      const fillStyle = apply(
        `bg-${colorKey(color, 600)}`,
        `hocus:bg-${colorKey(color, 500)}`,
        `active:bg-${colorKey(color, 700)}`,
        `text-${colorKey(color, 50)}`
      );

      const commonStyle = apply("flex items-center gap-2 rounded-sm");

      return [
        // common button
        [
          `.button-${color}`,
          {
            ...fillStyle,
            ...commonStyle,
            padding: `${padding._.y} ${padding._.x}`,
            ...apply(`rounded-sm`),
          },
        ],

        // icon button
        [
          `.icon-button-${color}`,
          {
            ...fillStyle,
            ...apply(
              `rounded-full inline-flex justify-center items-center p-3 text-lg`
            ),
          },
        ],

        // outline button
        [
          `.outline-button-${color}`,
          {
            ...commonStyle,
            padding: `calc(${padding._.y} - 2px) calc(${padding._.x} - 2px)`,
            ...apply(
              `border-2 border-${colorKey(color, 600)}`,
              `hocus:border-${colorKey(color, 500)}`,
              `active:border-${colorKey(color, 700)}`,
              `text-${colorKey(color, 800)}`,
              `hocus:text-${colorKey(color, 700)}`,
              `active:text-${colorKey(color, 900)}`,
              `active:bg-${colorKey(color, 50)}`
            ),
          },
        ],

        // outline button variants (reduce padding by border-width)
        [
          `.outline-button-${color}.button--sm`,
          {
            ...apply(`border`),
            padding: `calc(${padding.sm.y} - 1px) calc(${padding.sm.x} - 1px)`,
          },
        ],
        [
          `.outline-button-${color}.button--lg`,
          {
            ...apply(`border-4 rounded`),
            padding: `calc(${padding.lg.y} - 4px) calc(${padding.lg.x} - 4px)`,
          },
        ],
        [
          `.outline-button-${color}.button--xl`,
          {
            ...apply(`border-4 rounded-md`),
            padding: `calc(${padding.xl.y} - 4px) calc(${padding.xl.x} - 4px)`,
          },
        ],
      ];
    }),

    // button variants
    [
      `.button--sm`,
      {
        padding: `${padding.sm.y} ${padding.sm.x}`,
        ...apply(`text-sm gap-1.5`),
      },
    ],
    [
      `.button--lg`,
      {
        padding: `${padding.lg.y} ${padding.lg.x}`,
        ...apply(`text-lg font-medium gap-2.5`),
      },
    ],
    [
      `.button--xl`,
      {
        padding: `${padding.xl.y} ${padding.xl.x}`,
        ...apply(`text-xl flex-col font-semibold gap-4`),
      },
    ],
  ]);

  addComponents(components);

  function apply(...patterns) {
    return Object.fromEntries(
      patterns.map((pattern) => {
        const subPatterns = pattern.split(/\s+/).map((subPattern) => {
          if (/^[a-zA-Z]+:/.test(subPattern)) {
            const [mod, it] = subPattern.split(":", 2);
            return `${mod}:${prefix}${it}`;
          }
          return `${prefix}${subPattern}`;
        });
        return [`@apply ${subPatterns.join(" ")}`, {}];
      })
    );
  }

  function colorKey(colorName, ...tryKeys) {
    const colorDef = theme(`colors.${colorName}`);
    if (typeof colorDef === "string") {
      return colorName;
    }
    const keys = [...tryKeys.map((it) => it.toString()), "DEFAULT", "500"];
    for (const key of keys) {
      if (Reflect.has(colorDef, key)) {
        return `${colorName}-${key}`;
      }
    }
    return null;
  }
});
