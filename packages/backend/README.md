# Revently / Backend

## Setup

Copy and modify the environment variables as needed:

```shell
cp .env.proto .env
```

Install a `dotenv` tool,
like [python-dotenv](https://github.com/theskumar/python-dotenv/).

Alternatively, replace `dotenv` within the commands below with `source .env &&`.

### Database

Install the diesel cli that manages setup and migration:

```shell
cargo install diesel_cli --no-default-features --features "postgres"
```

Initialize the database:

```shell
dotenv run diesel database setup
```

## Build and run

Start the application with cargo:

```shell
cargo run
```

This should install all necessary dependencies and start the server.

## Maintenance

### Generate new database migration scaffold

```shell
dotenv run diesel migration generate <migration_name>
```

### Apply migrations

Apply new database migrations:

```shell
dotenv run diesel migration run
```
