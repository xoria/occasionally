use std::env;
use std::fmt::{Debug, Formatter};
use std::num::ParseIntError;
use std::str::{FromStr, ParseBoolError};

use dotenv::dotenv;
use once_cell::sync::Lazy;
use slog::{o, Drain, Logger};
use thiserror::Error;
use {slog_async, slog_envlogger, slog_term};

#[derive(Error, Debug)]
pub enum ConfigError {
    #[error(transparent)]
    ParseIntError(#[from] ParseIntError),
    #[error(transparent)]
    ParseBoolError(#[from] ParseBoolError),
}

#[derive(Debug)]
pub struct FeaturesConfig {
    pub email: bool,
}

#[derive(Debug)]
pub struct ServerConfig {
    pub host: String,
    pub port: u16,
}

pub struct SmtpConfig {
    pub host: String,
    pub user: String,
    pub password: String,
}

impl Debug for SmtpConfig {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "SmtpConfig {{ ")?;
        write!(
            f,
            "host: {:?} user: {:?}, password: ******",
            self.host, self.user
        )?;
        write!(f, " }}")
    }
}

#[derive(Debug)]
pub struct EmailConfig {
    pub from: String,
    pub reply_to: Option<String>,

    pub templates: EmailTemplates,
}

#[derive(Debug)]
pub struct EmailTemplates {
    pub register_verify: EmailTemplate,
    pub login_verify: EmailTemplate,
}

#[derive(Debug)]
pub struct EmailTemplate {
    pub subject: String,
    pub html: Option<String>,
    pub text: Option<String>,
}

#[derive(Debug)]
pub struct Config {
    pub features: FeaturesConfig,

    pub server: ServerConfig,
    pub postgres: tokio_postgres::Config,
    pub mailer: SmtpConfig,
    pub email: EmailConfig,

    pub debug: bool,
    pub dev: bool,
    pub pool_max_size: usize,
}

fn optional_string<Err>(str: Result<String, Err>) -> Option<String> {
    match str {
        Ok(value) => match value.len() {
            0 => None,
            _ => Some(value),
        },
        _ => None,
    }
}

fn parse_email_template_conf(name: &str) -> EmailTemplate {
    EmailTemplate {
        subject: env::var(format!("EMAIL.{}.SUBJECT", name)).unwrap(),
        text: optional_string(env::var(format!("EMAIL.{}.TEXT", name))),
        html: optional_string(env::var(format!("EMAIL.{}.HTML", name))),
    }
}

impl Config {
    pub fn from_env() -> Self {
        dotenv().ok();

        let mut pg_config = tokio_postgres::Config::new();
        pg_config.host(env::var("PGHOST").unwrap().as_str());
        pg_config
            .port(u16::from_str(env::var("PGPORT").unwrap().as_str()).unwrap());
        pg_config.dbname(env::var("PGDATABASE").unwrap().as_str());
        pg_config.user(env::var("PGUSER").unwrap().as_str());
        pg_config.password(env::var("PGPASSWORD").unwrap());

        Config {
            features: FeaturesConfig {
                email: env::var("ENABLE.EMAIL").unwrap().eq("true"),
            },

            server: ServerConfig {
                host: env::var("SERVER.HOST").unwrap(),
                port: u16::from_str(env::var("SERVER.PORT").unwrap().as_str())
                    .unwrap(),
            },
            postgres: pg_config,
            mailer: SmtpConfig {
                host: env::var("SMTP.HOST").unwrap(),
                user: env::var("SMTP.USER").unwrap(),
                password: env::var("SMTP.PASSWORD").unwrap(),
            },
            email: EmailConfig {
                from: env::var("EMAIL.FROM").unwrap(),
                reply_to: optional_string(env::var("EMAIL.REPLY_TO")),
                templates: EmailTemplates {
                    login_verify: parse_email_template_conf("LOGIN_VERIFY"),
                    register_verify: parse_email_template_conf(
                        "REGISTER_VERIFY",
                    ),
                },
            },

            debug: bool::from_str(env::var("SERVER.DEBUG").unwrap().as_str())
                .unwrap(),
            dev: bool::from_str(env::var("SERVER.DEV").unwrap().as_str())
                .unwrap(),
            pool_max_size: usize::from_str(
                env::var("PG_POOL.MAX_SIZE").unwrap().as_str(),
            )
            .unwrap(),
        }
    }

    pub fn configure_log() -> Logger {
        let decorator = slog_term::TermDecorator::new().build();
        let console_drain =
            slog_term::FullFormat::new(decorator).build().fuse();
        let console_drain = slog_envlogger::new(console_drain);
        let console_drain =
            slog_async::Async::new(console_drain).build().fuse();
        Logger::root(console_drain, o!("v" => env!("CARGO_PKG_VERSION")))
    }
}

pub static CONFIG: Lazy<Config> = Lazy::new(|| Config::from_env());
pub static LOG: Lazy<Logger> = Lazy::new(|| Config::configure_log());
