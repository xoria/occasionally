#![allow(dead_code)]

use std::fmt;

use serde::de::Error;
use serde::{Deserializer, Serializer};

use super::id11;

pub fn serialize<S>(id: &i64, serializer: S) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match id11::from_i64(id) {
        Ok(it) => serializer.serialize_str(it.as_str()),
        Err(err) => Err(serde::ser::Error::custom(err)),
    }
}

pub fn deserialize<'de, D>(deserializer: D) -> Result<i64, D::Error>
where
    D: Deserializer<'de>,
{
    struct Id11Deserializer;

    impl<'de> serde::de::Visitor<'de> for Id11Deserializer {
        type Value = i64;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a string containing an 11-char ID")
        }

        fn visit_str<E>(self, id: &str) -> Result<Self::Value, E>
        where
            E: Error,
        {
            match id11::to_i64(&id) {
                Ok(it) => Ok(it),
                Err(err) => Err(serde::de::Error::custom(err)),
            }
        }
    }

    deserializer.deserialize_str(Id11Deserializer)
}
