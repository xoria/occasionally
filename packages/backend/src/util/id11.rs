// TODO extract into a separate crate

// TODO use number input/output instead of hex (hex was useful in js
//      implementation, in rust it should be obsolete)

use std::collections::HashMap;

// universal constants
const HEX_BITS: u8 = 4;
const HEX: [char; 16] = [
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e',
    'f',
];
lazy_static! {
    static ref HEX_INVERSE: HashMap<char, usize> = HashMap::from_iter(
        HEX.iter()
            .enumerate()
            .map(|(idx, &char)| (char, idx))
            .collect::<Vec<_>>(),
    );
}

// alphabet specific constants
const ALPHABET: [char; 64] = [
    '-', '_', //
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', //
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
    'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', //
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
    'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
];
lazy_static! {
    static ref ALPHABET_INVERSE: HashMap<char, usize> = HashMap::from_iter(
        ALPHABET
            .iter()
            .enumerate()
            .map(|(idx, &char)| (char, idx))
            .collect::<Vec<_>>(),
    );
}
// const ALPHABET_INV: HashMap<char, usize> = HashMap::from([('a', 0)]);
const CHAR_BITS: u8 = 6; // 2 ^ 6 = 64

// use-case specific constants
const ID_CHARS: u8 = 11;
const ID_BITS: u8 = ID_CHARS * CHAR_BITS;
const HEX_COUNT: u8 = (ID_BITS + HEX_BITS - 1) / HEX_BITS; // ceil(ID_BITS / HEX_CHAR_BITS)
const FIRST_HEX_BITS: u8 = ID_BITS - (HEX_COUNT - 1) * HEX_BITS;

pub fn from_i64(input: &i64) -> Result<String, &'static str> {
    from_u64(&(*input as u64))
}

pub fn from_u64(input: &u64) -> Result<String, &'static str> {
    let hex_string = format!("{:x}", input);
    from_hex(hex_string)
}

/// Converts a lowercase hex id string (e.g. postgres result of `to_hex(int8)`)
/// to an id representation based on the URL-safe alphabet above with a fixed
/// length of `11`.
pub fn from_hex(hex: String) -> Result<String, &'static str> {
    let hex_bytes = hex.as_bytes();
    let mut hex_count = HEX_COUNT as usize;
    if hex_bytes.len() > hex_count {
        return Err("Invalid ID hex (bad size)");
    }

    let mut id_chars: Vec<char> = vec![];
    // the current remainder value
    let mut value = 0;
    // amount of bits used within `value`
    let mut bits = 0;
    let mut first_hex = true;

    // pad to expected length
    while hex.len() < hex_count {
        bits += if first_hex { FIRST_HEX_BITS } else { HEX_BITS };
        hex_count -= 1;
        first_hex = false;
    }
    while bits > CHAR_BITS {
        id_chars.push(ALPHABET[0]);
        bits -= CHAR_BITS;
    }

    for i in 0..hex_bytes.len() {
        let _bits = if first_hex { FIRST_HEX_BITS } else { HEX_BITS };
        value <<= _bits;
        bits += _bits;
        let ch = hex_bytes[i] as char;
        match HEX_INVERSE.get(&ch) {
            Some(it) => value += it,
            None => return Err("Invalid ID hex (bad character)"),
        }
        while bits > CHAR_BITS {
            let offset_bits = bits - CHAR_BITS;
            id_chars.push(ALPHABET[value >> offset_bits]);
            value &= (1 << offset_bits) - 1;
            bits -= CHAR_BITS;
        }
        first_hex = false;
    }

    if bits > 0 {
        id_chars.push(ALPHABET[value]);
    }

    Ok(id_chars.iter().collect())
}

pub fn to_i64(id: &str) -> Result<i64, &str> {
    to_u64(id).map(|it| it as i64)
}

pub fn to_u64(id: &str) -> Result<u64, &str> {
    match to_hex(id) {
        Ok(it) => Ok(u64::from_str_radix(&it, 16).unwrap()),
        Err(it) => Err(it),
    }
}

/// Converts an id representation string based on the URL-safe alphabet above,
/// with a fixed length of `11`, into a hex string (e.g. to be used as input for
/// postgres `int8`).
pub fn to_hex(id: &str) -> Result<String, &str> {
    let id_bytes = id.as_bytes();
    if id_bytes.len() != ID_CHARS as usize {
        return Err("Invalid ID (bad size)");
    }

    let mut hex_chars: Vec<char> = vec![];
    let mut value = 0;
    let mut bits = HEX_BITS - FIRST_HEX_BITS;

    for &ch in id_bytes {
        value <<= CHAR_BITS;
        match ALPHABET_INVERSE.get(&(ch as char)) {
            Some(it) => value += it,
            None => return Err("Invalid ID (bad character)"),
        }
        bits += CHAR_BITS;
        while bits > HEX_BITS {
            let offset_bits = bits - HEX_BITS;
            hex_chars.push(HEX[value >> offset_bits]);
            value &= (1 << offset_bits) - 1;
            bits -= HEX_BITS;
        }
    }

    if bits > 0 {
        hex_chars.push(HEX[value]);
    }

    Ok(hex_chars.iter().collect())
}

#[test]
fn test_convert_creates_identity() {
    _assert_id(0);
    _assert_id(10);
    _assert_id(-10);
    _assert_id(-10023);
    _assert_id(10023);
    _assert_id(7477526115806510);
    _assert_id(-7477526115806510);
    _assert_id(7477526115261806510);
    _assert_id(-7477526115261806510);
    _assert_id(i64::MAX);
    _assert_id(i64::MIN);
}

#[test]
fn test_from_hex_10() {
    let id11 = from_hex("a".to_string()).unwrap();
    assert_eq!(id11, "----------8");
    let id_hex = to_hex("----------8").unwrap();
    let id_hex = id_hex.trim_start_matches('0');
    assert_eq!(id_hex, "a");
}

#[test]
fn test_from_hex_7477526115261806510() {
    let id: i64 = 7477526115261806510;
    let id = format!("{:x}", id);
    let id11 = from_hex(id.clone()).unwrap();
    assert_eq!(id11, "4t3uA7oSJsI");
    let id_hex = to_hex("4t3uA7oSJsI").unwrap();
    let id_hex = id_hex.trim_start_matches('0');
    assert_eq!(id_hex, id);
}

#[test]
fn test_from_hex_negative_7477526115261806510() {
    let id: i64 = -7477526115261806510;
    let id = format!("{:x}", id);
    let id11 = from_hex(id.clone()).unwrap();
    assert_eq!(id11, "7uUtnQz5evg");
    let id_hex = to_hex("7uUtnQz5evg").unwrap();
    let id_hex = id_hex.trim_start_matches('0');
    assert_eq!(id_hex, id);
}

fn _assert_id(input: i64) {
    let id_hex = format!("{:x}", &input);
    let id11_over_hex = from_hex(id_hex.clone()).unwrap();
    assert_eq!(
        id_hex.trim_start_matches('0'),
        to_hex(&id11_over_hex).unwrap().trim_start_matches('0')
    );
    let id11_over_i64 = from_i64(&input).unwrap();
    assert_eq!(id11_over_i64, id11_over_hex);
    let id11_over_u64 = from_u64(&(input as u64)).unwrap();
    assert_eq!(id11_over_u64, id11_over_hex);
    assert_eq!(to_i64(&id11_over_hex).unwrap(), input);
    assert_eq!(to_u64(&id11_over_hex).unwrap(), input as u64);
}
