use deadpool_postgres::{ClientWrapper, Transaction};
use tokio_postgres::IsolationLevel;

use crate::api::error::Error;

pub async fn start(client: &mut ClientWrapper) -> Result<Transaction, Error> {
    let transaction = client
        .build_transaction()
        .isolation_level(IsolationLevel::Serializable)
        .start()
        .await?;
    Ok(transaction)
}
