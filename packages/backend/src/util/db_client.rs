use deadpool_postgres::{ClientWrapper, Transaction};
use tokio_postgres::types::ToSql;
use tokio_postgres::{Client, GenericClient, Row, Statement, ToStatement};

use crate::api::error::{Error, ServerError};

enum DbClientInner<'a> {
    Client(&'a ClientWrapper),
    Transaction(&'a Transaction<'a>),
}

pub struct DbClient<'a> {
    inner: DbClientInner<'a>,
}

impl<'a> From<&'a ClientWrapper> for DbClient<'a> {
    fn from(value: &'a ClientWrapper) -> Self {
        DbClient {
            inner: DbClientInner::Client(value),
        }
    }
}

impl<'a> From<&'a Transaction<'a>> for DbClient<'a> {
    fn from(value: &'a Transaction) -> Self {
        DbClient {
            inner: DbClientInner::Transaction(value),
        }
    }
}

impl DbClient<'_> {
    /// Return the underlying [Client]
    pub fn _raw(&self) -> &Client {
        match self.inner {
            DbClientInner::Client(it) => it.client(),
            DbClientInner::Transaction(it) => it.client(),
        }
    }

    pub fn assert_transaction(&self) -> Result<(), Error> {
        match self.inner {
            DbClientInner::Client(_) => {
                Err(Error::from(ServerError::NoTransaction))
            }
            DbClientInner::Transaction(_) => Ok(()),
        }
    }

    /// Same as [ClientWrapper::prepare_cached]
    pub async fn prepare_cached(
        &self,
        query: &str,
    ) -> Result<Statement, tokio_postgres::Error> {
        match self.inner {
            DbClientInner::Client(it) => it.prepare_cached(query).await,
            DbClientInner::Transaction(it) => it.prepare_cached(query).await,
        }
    }

    /// Same as [Client::query]
    pub async fn query<T>(
        &self,
        statement: &T,
        params: &[&(dyn ToSql + Sync)],
    ) -> Result<Vec<Row>, tokio_postgres::Error>
    where
        T: ?Sized + ToStatement,
    {
        self._raw().query(statement, params).await
    }

    /// Same as [Client::query_one]
    pub async fn query_one<T>(
        &self,
        statement: &T,
        params: &[&(dyn ToSql + Sync)],
    ) -> Result<Row, tokio_postgres::Error>
    where
        T: ?Sized + ToStatement,
    {
        self._raw().query_one(statement, params).await
    }

    /// Same as [Client::query_opt]
    pub async fn query_opt<T>(
        &self,
        statement: &T,
        params: &[&(dyn ToSql + Sync)],
    ) -> Result<Option<Row>, tokio_postgres::Error>
    where
        T: ?Sized + ToStatement,
    {
        self._raw().query_opt(statement, params).await
    }

    /// Same as [Client::execute]
    pub async fn execute<T>(
        &self,
        statement: &T,
        params: &[&(dyn ToSql + Sync)],
    ) -> Result<u64, tokio_postgres::Error>
    where
        T: ?Sized + ToStatement,
    {
        self._raw().execute(statement, params).await
    }
}
