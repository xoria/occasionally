#![allow(dead_code)]

use std::fmt;

use serde::de::Error;
use serde::{Deserializer, Serializer};

use crate::util::id11_serde;

pub fn serialize<S>(
    id_opt: &Option<i64>,
    serializer: S,
) -> Result<S::Ok, S::Error>
where
    S: Serializer,
{
    match id_opt {
        Some(id) => id11_serde::serialize(id, serializer),
        None => serializer.serialize_none(),
    }
}

pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<i64>, D::Error>
where
    D: Deserializer<'de>,
{
    struct Id11OptDeserializer;

    impl<'de> serde::de::Visitor<'de> for Id11OptDeserializer {
        type Value = Option<i64>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("an optional string containing an 11-char ID")
        }

        fn visit_none<E>(self) -> Result<Self::Value, E>
        where
            E: Error,
        {
            Ok(None)
        }

        fn visit_some<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
        where
            D: Deserializer<'de>,
        {
            Ok(Some(id11_serde::deserialize(deserializer)?))
        }
    }

    deserializer.deserialize_option(Id11OptDeserializer)
}
