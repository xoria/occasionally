/// Creates a diff of the two passed vectors, returning a tuple
/// `(removed_values, added_values)`
pub fn list_diff<'a, T>(
    old_value: &'a Vec<T>,
    new_value: &'a Vec<T>,
) -> (Vec<&'a T>, Vec<&'a T>)
where
    T: Eq,
{
    let mut removed: Vec<&'a T> = vec![];
    let mut added: Vec<&'a T> = vec![];
    for current_old in old_value {
        if !new_value.contains(&current_old) {
            removed.push(&current_old);
        }
    }
    for current_new in new_value {
        if !old_value.contains(&current_new) {
            added.push(&current_new);
        }
    }
    (removed, added)
}
