use once_cell::sync::Lazy;
use regex::{Captures, Regex};
use serde::Serialize;
use serde_json::{Map, Value};

use crate::config::EmailTemplate;

static CURLY_MATCHER: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"\{\{ *([a-zA-Z0-9_]+) *}}").unwrap());

fn _tpl_replace(tpl: &str, map: &Map<String, Value>) -> String {
    CURLY_MATCHER
        .replace_all(tpl, |captures: &Captures| {
            let result = map.get(&captures[1]);
            match result {
                Some(value) => value.to_string(),
                None => captures[0].to_string(),
            }
        })
        .to_string()
}

fn _to_map(data: &impl Serialize) -> Map<String, Value> {
    let data_json = serde_json::to_value(data).unwrap();
    data_json.as_object().unwrap().to_owned()
}

#[allow(dead_code)]
pub fn template_replace(template: &str, data: &impl Serialize) -> String {
    _tpl_replace(template, &_to_map(data))
}

pub fn template_replace_email(
    tpl: &EmailTemplate,
    data: &impl Serialize,
) -> EmailTemplate {
    let map = _to_map(data);
    EmailTemplate {
        subject: _tpl_replace(tpl.subject.as_str(), &map).to_string(),
        text: tpl
            .text
            .as_ref()
            .map(|it| _tpl_replace(it.as_str(), &map).to_string()),
        html: tpl
            .html
            .as_ref()
            .map(|it| _tpl_replace(it.as_str(), &map).to_string()),
    }
}
