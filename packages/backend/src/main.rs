#[macro_use]
extern crate lazy_static;
extern crate slog;
extern crate slog_async;
extern crate slog_term;

use actix_web::middleware::TrailingSlash;
use actix_web::{middleware, web, App, HttpServer};
use deadpool_postgres::{Manager, ManagerConfig, Pool, RecyclingMethod};
use slog::info;
use tokio_postgres::NoTls;

use crate::app_state::AppState;
use crate::config::{CONFIG, LOG};

mod api;
mod core;

mod app_state;
mod config;
mod util;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv::dotenv().ok();

    if CONFIG.debug {
        info!(LOG, "{:?}", CONFIG);
    }
    info!(
        LOG,
        "Starting server at http://{}:{}",
        CONFIG.server.host,
        CONFIG.server.port
    );

    let mgr_config = ManagerConfig {
        recycling_method: RecyclingMethod::Fast,
    };
    let mgr =
        Manager::from_config(CONFIG.postgres.to_owned(), NoTls, mgr_config);
    let pool = Pool::builder(mgr)
        .max_size(CONFIG.pool_max_size)
        .build()
        .unwrap();

    HttpServer::new(move || {
        App::new()
            .app_data(web::Data::new(AppState {
                pool: pool.clone(),
                log: LOG.clone(),
            }))
            .wrap(middleware::NormalizePath::new(TrailingSlash::Trim))
            .wrap(middleware::Logger::default())
            .service(api::v1::create())
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}
