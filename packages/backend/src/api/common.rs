use serde_derive::Deserialize;

#[derive(Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum AccessQueryKind {
    Personal,
}

#[derive(Deserialize)]
pub struct AccessQuery {
    pub kind: Option<AccessQueryKind>,
}
