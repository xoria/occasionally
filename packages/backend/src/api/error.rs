use std::backtrace::Backtrace;
use std::fmt::{Debug, Display, Formatter};

use actix_web::body::BoxBody;
use actix_web::http::header::ContentType;
use actix_web::http::StatusCode;
use actix_web::{HttpResponse, ResponseError};
use hex::FromHexError;
use slog::{error, warn};

use crate::config::LOG;
use crate::core::mail::error::MailError;

#[derive(Debug)]
#[allow(dead_code)]
pub enum ClientError {
    Unauthorized,
    Forbidden,
    NotFound,
    BadRequest(String),
    PayloadTooLarge,
    ImATeapot,
    TooManyRequests,
    FromHexError(FromHexError),
}

impl ClientError {
    fn http_status_code(&self) -> StatusCode {
        match self {
            ClientError::Unauthorized => StatusCode::UNAUTHORIZED,
            ClientError::Forbidden => StatusCode::FORBIDDEN,
            ClientError::NotFound => StatusCode::NOT_FOUND,
            ClientError::BadRequest(_) => StatusCode::BAD_REQUEST,
            ClientError::PayloadTooLarge => StatusCode::PAYLOAD_TOO_LARGE,
            ClientError::ImATeapot => StatusCode::IM_A_TEAPOT,
            ClientError::TooManyRequests => StatusCode::TOO_MANY_REQUESTS,
            ClientError::FromHexError(_) => StatusCode::BAD_REQUEST,
        }
    }
}

#[derive(Debug)]
pub enum ServerError {
    NoTransaction,
    InvalidState,
    FromSqlEnumError(String),
    DatabaseError(tokio_postgres::Error),
    DatabaseError2(tokio_pg_mapper::Error),
    MailError(MailError),
}

impl ServerError {
    fn http_status_code(&self) -> StatusCode {
        StatusCode::INTERNAL_SERVER_ERROR
    }
}

#[derive(Debug)]
enum ErrorKind {
    ClientError(ClientError),
    ServerError(ServerError),
}

impl ErrorKind {
    fn http_status_code(&self) -> StatusCode {
        match self {
            ErrorKind::ClientError(err) => err.http_status_code(),
            ErrorKind::ServerError(err) => err.http_status_code(),
        }
    }
}

#[derive(Debug)]
pub struct Error {
    kind: ErrorKind,
    backtrace: Backtrace,
}

impl Error {
    fn new(kind: ErrorKind) -> Error {
        Error {
            kind,
            backtrace: Backtrace::capture(),
        }
    }
}

impl Display for ClientError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            ClientError::Unauthorized => f.write_str("Unauthorized"),
            ClientError::Forbidden => f.write_str("Forbidden"),
            ClientError::NotFound => f.write_str("NotFound"),
            ClientError::BadRequest(msg) => {
                f.write_str(&format!("BadRequest: {}", msg))
            }
            ClientError::PayloadTooLarge => f.write_str("PayloadTooLarge"),
            ClientError::ImATeapot => f.write_str("ImATeapot"),
            ClientError::TooManyRequests => f.write_str("TooManyRequests"),
            ClientError::FromHexError(err) => Display::fmt(&err, f),
        }
    }
}

impl Display for ServerError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            ServerError::NoTransaction => f.write_str("Expected DB accessor to be a transaction. Found raw client instead."),
            ServerError::InvalidState => f.write_str("InvalidState"),
            ServerError::FromSqlEnumError(value) => f.write_str(&format!("Invalid enum value: {}", value)),
            ServerError::DatabaseError(err) => Display::fmt(&err, f),
            ServerError::DatabaseError2(err) => Display::fmt(&err, f),
            ServerError::MailError(err) => Display::fmt(&err, f),
        }
    }
}

impl Display for ErrorKind {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            ErrorKind::ClientError(err) => Display::fmt(&err, f),
            ErrorKind::ServerError(err) => Display::fmt(&err, f),
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        f.write_fmt(format_args!(
            "Application Error {{ kind: {:?}, backtrace: [below] }}\n{}",
            self.kind, self.backtrace
        ))
    }
}

impl std::error::Error for ClientError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            ClientError::FromHexError(err) => Some(err),
            _ => None,
        }
    }
}

impl std::error::Error for ServerError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            ServerError::DatabaseError(err) => Some(err),
            ServerError::MailError(err) => Some(err),
            _ => None,
        }
    }
}

pub fn assert_some<T>(value: Option<T>) -> Result<T, Error> {
    value.ok_or(Error::new(ErrorKind::ClientError(ClientError::NotFound)))
}

pub fn assert_some_or<T>(
    value: Option<T>,
    error: impl Into<Error>,
) -> Result<T, Error> {
    match value {
        Some(it) => Ok(it),
        None => Err(error.into()),
    }
}

impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse<BoxBody> {
        match self.kind {
            ErrorKind::ServerError(_) => error!(LOG, "{}", self),
            ErrorKind::ClientError(_) => warn!(LOG, "{}", self),
        }

        let status = self.kind.http_status_code();

        let mut res = HttpResponse::build(status);
        res.insert_header(ContentType::plaintext());
        res.body(())
    }
}

impl From<ClientError> for Error {
    fn from(value: ClientError) -> Self {
        Error::new(ErrorKind::ClientError(value))
    }
}

impl From<ServerError> for Error {
    fn from(value: ServerError) -> Self {
        Error::new(ErrorKind::ServerError(value))
    }
}

impl From<tokio_postgres::Error> for Error {
    fn from(value: tokio_postgres::Error) -> Self {
        Error::from(ServerError::DatabaseError(value))
    }
}

impl From<tokio_pg_mapper::Error> for Error {
    fn from(value: tokio_pg_mapper::Error) -> Self {
        Error::from(ServerError::DatabaseError2(value))
    }
}

impl From<MailError> for Error {
    fn from(value: MailError) -> Self {
        Error::from(ServerError::MailError(value))
    }
}

impl From<FromHexError> for Error {
    fn from(value: FromHexError) -> Self {
        Error::from(ClientError::FromHexError(value))
    }
}
