use actix_web::{web, Scope};

use crate::core::{account, peer, revent, session};

pub fn create() -> Scope {
    web::scope("/v1")
        .service(account::control::routes())
        .service(peer::control::routes_peer())
        .service(peer::control::routes_peer_group())
        .service(revent::control::routes())
        .service(session::control::routes())
}
