use deadpool_postgres::Pool;
use slog::Logger;

#[derive(Clone)]
pub struct AppState {
    pub pool: Pool,
    pub log: Logger,
}
