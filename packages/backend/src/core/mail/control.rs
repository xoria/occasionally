use lettre::message::header::ContentType;
use lettre::message::{header, MessageBuilder, MultiPart, SinglePart};
use lettre::transport::smtp;
use lettre::{SmtpTransport, Transport};
use once_cell::sync::Lazy;
use slog::debug;
use smtp::authentication::Credentials;
use smtp::response::Response;

use crate::config::{EmailTemplate, CONFIG, LOG};
use crate::core::mail::error::MailError;

static CREDS: Lazy<Credentials> = Lazy::new(|| {
    Credentials::new(
        CONFIG.mailer.user.to_string(),
        CONFIG.mailer.password.to_string(),
    )
});
static MAILER: Lazy<SmtpTransport> = Lazy::new(|| {
    SmtpTransport::relay(CONFIG.mailer.host.as_str())
        .unwrap()
        .credentials(CREDS.to_owned())
        .build()
});

pub fn send_mail(
    template: &EmailTemplate,
    to: &String,
) -> Result<Option<Response>, MailError> {
    let mut builder = MessageBuilder::new()
        .from(CONFIG.email.from.parse()?)
        .to(to.parse()?)
        .subject(template.subject.to_owned());
    if let Some(it) = &CONFIG.email.reply_to {
        builder = builder.reply_to(it.parse()?);
    }
    let text_part: Option<SinglePart> = template.text.as_ref().map(|text| {
        SinglePart::builder()
            .header(ContentType::TEXT_PLAIN)
            .body(text.to_string())
    });
    let html_part: Option<SinglePart> = template.html.as_ref().map(|html| {
        SinglePart::builder()
            .header(header::ContentType::TEXT_HTML)
            .body(html.to_string())
    });

    let message = if let Some(text) = text_part {
        if let Some(html) = html_part {
            builder.multipart(
                MultiPart::alternative().singlepart(text).singlepart(html),
            )?
        } else {
            builder.singlepart(text)?
        }
    } else if let Some(html) = html_part {
        builder.singlepart(html)?
    } else {
        return Err(MailError::NoTemplateDefined);
    };

    debug!(
        LOG,
        "Send email from template: {:?}; {:?}", template, message
    );

    if CONFIG.features.email {
        Ok(Some(MAILER.send(&message)?))
    } else {
        Ok(None)
    }
}
