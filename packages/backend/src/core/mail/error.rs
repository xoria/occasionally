use std::error::Error as StdError;
use std::fmt::{Debug, Display, Formatter};

use lettre::transport::smtp;

#[derive(Debug)]
pub enum MailError {
    NoTemplateDefined,
    InvalidAddress(lettre::address::AddressError),
    BuilderFailed(smtp::Error),
    SendFailed(lettre::error::Error),
}

impl Display for MailError {
    fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
        match self {
            MailError::NoTemplateDefined => f.write_str("NoTemplateDefined"),
            MailError::InvalidAddress(err) => std::fmt::Display::fmt(&err, f),
            MailError::BuilderFailed(err) => std::fmt::Display::fmt(&err, f),
            MailError::SendFailed(err) => std::fmt::Display::fmt(&err, f),
        }
    }
}

impl StdError for MailError {
    fn source(&self) -> Option<&(dyn StdError + 'static)> {
        match self {
            MailError::NoTemplateDefined => None,
            MailError::InvalidAddress(err) => Some(err),
            MailError::BuilderFailed(err) => Some(err),
            MailError::SendFailed(err) => Some(err),
        }
    }
}

impl From<lettre::address::AddressError> for MailError {
    fn from(err: lettre::address::AddressError) -> Self {
        MailError::InvalidAddress(err)
    }
}

impl From<smtp::Error> for MailError {
    fn from(err: smtp::Error) -> Self {
        MailError::BuilderFailed(err)
    }
}

impl From<lettre::error::Error> for MailError {
    fn from(err: lettre::error::Error) -> Self {
        MailError::SendFailed(err)
    }
}
