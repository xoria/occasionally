use actix_web::cookie::time::Duration;
use actix_web::cookie::{Cookie, SameSite};
use actix_web::HttpRequest;
use once_cell::sync::Lazy;

use crate::api::error::{assert_some, Error};
use crate::config::CONFIG;
use crate::core::account;
use crate::core::account::model::AccountBo;
use crate::core::mail::control::send_mail;
use crate::core::session::model::{
    LoginInitBo, LoginInitReq, LoginReq, RegisterInitBo, RegisterInitReq,
    RegisterReq, TemplateInfoVerifyCode,
};
use crate::core::session::repository;
use crate::util::db_client::DbClient;
use crate::util::template::template_replace_email;

static AUTH_COOKIE_NAME: Lazy<&str> = Lazy::new(|| {
    if CONFIG.dev {
        "DEV__RVT-Authorization"
    } else {
        "__Host-RVT-Authorization"
    }
});

pub async fn get_session_account(
    client: &DbClient<'_>,
    req: &HttpRequest,
) -> Result<Option<AccountBo>, Error> {
    let result = match parse_auth_cookie(req)? {
        Some(token) => {
            account::service::resolve_session_token(client, &token).await?
        }
        None => None,
    };
    Ok(result)
}

pub(super) fn parse_auth_cookie(
    req: &HttpRequest,
) -> Result<Option<Vec<u8>>, Error> {
    let auth_cookie = req.cookie(*AUTH_COOKIE_NAME);
    let result = match auth_cookie {
        Some(cookie) => Some(hex::decode(cookie.value())?),
        None => None,
    };
    Ok(result)
}

pub(super) fn make_auth_cookie<'a>(token: Option<Vec<u8>>) -> Cookie<'a> {
    let mut auth_cookie = match &token {
        Some(it) => Cookie::new(*AUTH_COOKIE_NAME, hex::encode(it)),
        None => Cookie::new(*AUTH_COOKIE_NAME, ""),
    };

    auth_cookie.set_same_site(SameSite::Lax);
    auth_cookie.set_path("/");
    auth_cookie.set_http_only(true);

    match &token {
        Some(_) => auth_cookie.set_max_age(Duration::weeks(12)),
        None => auth_cookie.make_removal(),
    }

    auth_cookie
}

pub(super) async fn send_register_token(
    client: &DbClient<'_>,
    req: RegisterInitReq,
) -> Result<RegisterInitBo, Error> {
    let register_init = repository::create_register_token(client, req).await?;

    let template = template_replace_email(
        &CONFIG.email.templates.register_verify,
        &TemplateInfoVerifyCode::from(&register_init),
    );
    send_mail(&template, &register_init.email)?;

    Ok(register_init)
}

pub(super) async fn send_login_token(
    client: &DbClient<'_>,
    req: LoginInitReq,
) -> Result<LoginInitBo, Error> {
    let account =
        assert_some(account::service::find_account(client, &req.email).await?)?;
    let login_init =
        repository::create_login_token(client, &account.id).await?;

    let template = template_replace_email(
        &CONFIG.email.templates.login_verify,
        &TemplateInfoVerifyCode::from(&login_init),
    );
    send_mail(&template, &account.email)?;

    Ok(login_init)
}

pub(super) async fn assert_register_token(
    client: &DbClient<'_>,
    req: RegisterReq,
) -> Result<String, Error> {
    let result =
        assert_some(repository::check_register_token(client, &req).await?)?;
    repository::drop_register_token(client, &req.client_rnd).await?;

    Ok(result)
}

pub(super) async fn assert_login_token(
    client: &DbClient<'_>,
    req: LoginReq,
) -> Result<i32, Error> {
    let result =
        assert_some(repository::check_login_token(client, &req).await?)?;
    repository::drop_login_token(client, &req.client_rnd).await?;

    Ok(result)
}

pub(super) async fn create_session_token(
    client: &DbClient<'_>,
    account_id: &i32,
) -> Result<Vec<u8>, Error> {
    Ok(repository::create_session_token(client, account_id).await?)
}

pub(super) async fn drop_session_token(
    client: &DbClient<'_>,
    token: &Vec<u8>,
) -> Result<(), Error> {
    Ok(repository::drop_session_token(client, token).await?)
}
