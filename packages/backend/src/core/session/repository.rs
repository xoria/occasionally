use tokio_pg_mapper::{Error, FromTokioPostgresRow};

use super::model::{RegisterInitBo, RegisterInitReq, RegisterReq};
use crate::core::session::model::{LoginInitBo, LoginReq};
use crate::util::db_client::DbClient;

pub async fn create_register_token(
    client: &DbClient<'_>,
    req: RegisterInitReq,
) -> Result<RegisterInitBo, Error> {
    let stmt = client
        .prepare_cached(
            "INSERT INTO temp_token_register(email) VALUES($1) RETURNING *",
        )
        .await?;
    let row = client.query_one(&stmt, &[&req.email]).await?;
    RegisterInitBo::from_row(row)
}

pub async fn create_login_token(
    client: &DbClient<'_>,
    account_id: &i32,
) -> Result<LoginInitBo, Error> {
    let stmt = client
        .prepare_cached(
            "INSERT INTO temp_token_login(account_id) VALUES($1) RETURNING *",
        )
        .await?;
    let row = client.query_one(&stmt, &[account_id]).await?;
    LoginInitBo::from_row(row)
}

pub async fn check_register_token(
    client: &DbClient<'_>,
    req: &RegisterReq,
) -> Result<Option<String>, Error> {
    let stmt = client
        .prepare_cached(
            "\
            SELECT email \
            FROM temp_token_register \
            WHERE client_rnd=$1 \
                AND token=$2\
            ",
        )
        .await?;
    let row = client
        .query_opt(&stmt, &[&req.client_rnd, &req.code])
        .await?;
    Ok(row.map(|it| it.get(0)))
}

pub async fn check_login_token(
    client: &DbClient<'_>,
    req: &LoginReq,
) -> Result<Option<i32>, Error> {
    let stmt = client
        .prepare_cached(
            "\
            SELECT account_id \
            FROM temp_token_login \
            WHERE client_rnd=$1 \
                AND token=$2\
            ",
        )
        .await?;
    let row = client
        .query_opt(&stmt, &[&req.client_rnd, &req.code])
        .await?;
    Ok(row.map(|it| it.get(0)))
}

pub async fn drop_register_token(
    client: &DbClient<'_>,
    client_rnd: &i64,
) -> Result<(), Error> {
    let stmt = client
        .prepare_cached("DELETE FROM temp_token_register WHERE client_rnd=$1")
        .await?;
    client.execute(&stmt, &[client_rnd]).await?;
    Ok(())
}

pub async fn drop_login_token(
    client: &DbClient<'_>,
    client_rnd: &i64,
) -> Result<(), Error> {
    let stmt = client
        .prepare_cached("DELETE FROM temp_token_login WHERE client_rnd=$1")
        .await?;
    client.execute(&stmt, &[client_rnd]).await?;
    Ok(())
}

pub async fn create_session_token(
    client: &DbClient<'_>,
    account_id: &i32,
) -> Result<Vec<u8>, Error> {
    // we don't set date_expire as default within database as we most likely
    // will create tokens of different duration and roles later on...
    let stmt = client
        .prepare_cached(
            "\
            INSERT INTO session_token(account_id, date_expire) \
            VALUES($1, now() + interval '12weeks') \
            RETURNING id\
            ",
        )
        .await?;
    let row = client.query_one(&stmt, &[account_id]).await?;
    Ok(row.get(0))
}

pub async fn drop_session_token(
    client: &DbClient<'_>,
    token: &Vec<u8>,
) -> Result<(), Error> {
    let stmt = client
        .prepare_cached("DELETE FROM session_token WHERE id=$1")
        .await?;
    client.execute(&stmt, &[token]).await?;
    Ok(())
}
