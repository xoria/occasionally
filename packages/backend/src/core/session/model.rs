use serde_derive::{Deserialize, Serialize};
use tokio_pg_mapper_derive::PostgresMapper;

use crate::util::id11_serde;

#[derive(PostgresMapper, Serialize, Debug)]
#[pg_mapper(table = "temp_token_register")]
#[serde(rename_all = "camelCase")]
pub struct RegisterInitBo {
    #[serde(with = "id11_serde")]
    pub client_rnd: i64,
    #[serde(skip)]
    pub token: String,
    #[serde(skip)]
    pub email: String,
}

#[derive(PostgresMapper, Serialize, Debug)]
#[pg_mapper(table = "temp_token_login")]
#[serde(rename_all = "camelCase")]
pub struct LoginInitBo {
    #[serde(with = "id11_serde")]
    pub client_rnd: i64,
    #[serde(skip)]
    pub token: String,
    #[serde(skip)]
    pub account_id: i32,
}

#[derive(Serialize)]
pub struct TemplateInfoVerifyCode {
    pub code: String,
    pub minutes: i32,
}

impl From<&RegisterInitBo> for TemplateInfoVerifyCode {
    fn from(init: &RegisterInitBo) -> TemplateInfoVerifyCode {
        TemplateInfoVerifyCode {
            code: init.token.clone(),
            minutes: 30,
        }
    }
}

impl From<&LoginInitBo> for TemplateInfoVerifyCode {
    fn from(init: &LoginInitBo) -> TemplateInfoVerifyCode {
        TemplateInfoVerifyCode {
            code: init.token.clone(),
            minutes: 60,
        }
    }
}

#[derive(Deserialize)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct RegisterInitReq {
    pub email: String,
}

pub type LoginInitReq = RegisterInitReq;

#[derive(Deserialize, Debug)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct RegisterReq {
    #[serde(with = "id11_serde")]
    pub client_rnd: i64,
    pub code: String,
}

pub type LoginReq = RegisterReq;
