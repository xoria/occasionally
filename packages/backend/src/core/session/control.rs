use actix_web::cookie::Cookie;
use actix_web::web::Json;
use actix_web::{get, post, web, HttpRequest, HttpResponse, Scope};
use futures::TryFutureExt;
use slog::{debug, trace};

use crate::api::error::{Error, ServerError};
use crate::app_state::AppState;
use crate::core::account;
use crate::core::account::model::{AccountBo, CreateAccountBo};
use crate::core::session::model::{
    LoginInitReq, LoginReq, RegisterInitReq, RegisterReq,
};
use crate::core::session::service;
use crate::util::db_client::DbClient;
use crate::util::transaction;

pub fn routes() -> Scope {
    web::scope("/session")
        .service(register_init)
        .service(register)
        .service(login_init)
        .service(login)
        .service(logout)
        .service(session_self)
}

#[post("/register/init")]
async fn register_init(
    state: web::Data<AppState>,
    body: Json<RegisterInitReq>,
) -> Result<HttpResponse, Error> {
    trace!(state.log, "Start new registration init");
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let register_init =
        service::send_register_token(client, body.into_inner()).await?;

    debug!(
        state.log,
        "Initialize new registration: {:?}", register_init
    );

    Ok(HttpResponse::Ok().json(register_init))
}

#[post("/login/init")]
async fn login_init(
    state: web::Data<AppState>,
    body: Json<LoginInitReq>,
) -> Result<HttpResponse, Error> {
    trace!(state.log, "Start new login init");
    let client = &mut *state.pool.get().await.unwrap();
    let transaction = transaction::start(client).await?;
    let client = &DbClient::from(&transaction);

    let login_init =
        service::send_login_token(client, body.into_inner()).await?;

    transaction.commit().await?;
    debug!(state.log, "Initialize new login: {:?}", login_init);

    Ok(HttpResponse::Ok().json(login_init))
}

#[post("/register")]
async fn register(
    state: web::Data<AppState>,
    body: Json<RegisterReq>,
) -> Result<HttpResponse, Error> {
    trace!(state.log, "Start actual registration {:?}", body);
    let client = &mut *state.pool.get().await.unwrap();
    let transaction = transaction::start(client).await?;
    let client = &DbClient::from(&transaction);

    let account_id = service::assert_register_token(client, body.into_inner())
        .and_then(|email| {
            account::service::create_account(client, CreateAccountBo { email })
        })
        .await?;
    let (auth_cookie, account) = handle_login(client, &account_id).await?;

    transaction.commit().await?;
    debug!(state.log, "Account#{} registered successfully", account_id);

    Ok(HttpResponse::Ok().cookie(auth_cookie).json(account))
}

#[post("/login")]
async fn login(
    state: web::Data<AppState>,
    body: Json<LoginReq>,
) -> Result<HttpResponse, Error> {
    trace!(state.log, "Start actual login {:?}", body);
    let client = &mut *state.pool.get().await.unwrap();
    let transaction = transaction::start(client).await?;
    let client = &DbClient::from(&transaction);

    let account_id =
        service::assert_login_token(client, body.into_inner()).await?;
    let (auth_cookie, account) = handle_login(client, &account_id).await?;

    transaction.commit().await?;
    Ok(HttpResponse::Ok().cookie(auth_cookie).json(account))
}

async fn handle_login<'a>(
    client: &DbClient<'_>,
    account_id: &i32,
) -> Result<(Cookie<'a>, AccountBo), Error> {
    let token = service::create_session_token(client, account_id).await?;
    let account = account::service::resolve_session_token(client, &token)
        .await?
        .ok_or(ServerError::InvalidState)?;

    Ok((service::make_auth_cookie(Some(token)), account))
}

#[post("/logout")]
async fn logout(
    state: web::Data<AppState>,
    req: HttpRequest,
) -> Result<HttpResponse, Error> {
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    match service::parse_auth_cookie(&req)? {
        Some(token) => {
            service::drop_session_token(client, &token).await?;
            let cookie = service::make_auth_cookie(None);
            Ok(HttpResponse::NoContent().cookie(cookie).finish())
        }
        None => Ok(HttpResponse::NoContent().finish()),
    }
}

#[get("/self")]
async fn session_self(
    state: web::Data<AppState>,
    req: HttpRequest,
) -> Result<HttpResponse, Error> {
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let account = service::get_session_account(client, &req).await?;

    match account {
        Some(account) => {
            debug!(state.log, "Client authorized for Account#{}", account.id);
            Ok(HttpResponse::Ok().json(account))
        }
        None => {
            debug!(state.log, "Client not authorized");
            Ok(HttpResponse::NoContent().finish())
        }
    }
}
