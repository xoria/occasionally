use actix_web::web::{Json, Path};
use actix_web::{delete, get, put, web, HttpRequest, HttpResponse, Scope};
use slog::{debug, trace};

use crate::api::error::{assert_some_or, ClientError, Error};
use crate::app_state::AppState;
use crate::core::peer::model::{PeerGroupUpsertReq, PeerUpsertReq};
use crate::core::peer::service;
use crate::core::session;
use crate::util::db_client::DbClient;
use crate::util::transaction;

pub fn routes_peer() -> Scope {
    web::scope("/peer")
        .service(list_peers)
        .service(upsert_peer)
        .service(delete_peer)
}
pub fn routes_peer_group() -> Scope {
    web::scope("/peer-group")
        .service(list_peer_groups)
        .service(upsert_peer_group)
        .service(delete_peer_group)
}

#[get("")]
async fn list_peers(
    state: web::Data<AppState>,
    req: HttpRequest,
) -> Result<HttpResponse, Error> {
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let account = session::service::get_session_account(&client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    trace!(state.log, "List peers");

    let peers = service::list_peers(&client, &account.id).await?;

    Ok(HttpResponse::Ok().json(peers))
}

#[put("")]
async fn upsert_peer(
    state: web::Data<AppState>,
    body: Json<PeerUpsertReq>,
    req: HttpRequest,
) -> Result<HttpResponse, Error> {
    let client = &mut *state.pool.get().await.unwrap();
    let transaction = transaction::start(client).await?;
    let client = &DbClient::from(&transaction);

    let account = session::service::get_session_account(&client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    trace!(state.log, "Upsert peer: {:?}", body);

    let peer =
        service::upsert_peer(&client, &account.id, body.into_inner()).await?;

    transaction.commit().await?;
    debug!(state.log, "Peer updated: {:?}", peer);

    Ok(HttpResponse::Ok().json(peer))
}

#[delete("/{id}")]
async fn delete_peer(
    state: web::Data<AppState>,
    path: Path<i32>,
    req: HttpRequest,
) -> Result<HttpResponse, Error> {
    let peer_id = path.into_inner();
    let client = &mut *state.pool.get().await.unwrap();
    let transaction = transaction::start(client).await?;
    let client = &DbClient::from(&transaction);

    let account = session::service::get_session_account(&client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    trace!(state.log, "Delete Peer#{}", peer_id);

    service::delete_peer(&client, &account.id, &peer_id).await?;

    transaction.commit().await?;
    debug!(state.log, "Peer#{} deleted", peer_id);

    Ok(HttpResponse::NoContent().finish())
}

#[get("")]
async fn list_peer_groups(
    state: web::Data<AppState>,
    req: HttpRequest,
) -> Result<HttpResponse, Error> {
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let account = session::service::get_session_account(&client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    trace!(state.log, "List peer groups");

    let peer_groups = service::list_peer_groups(&client, &account.id).await?;

    Ok(HttpResponse::Ok().json(peer_groups))
}

#[put("")]
async fn upsert_peer_group(
    state: web::Data<AppState>,
    body: Json<PeerGroupUpsertReq>,
    req: HttpRequest,
) -> Result<HttpResponse, Error> {
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let account = session::service::get_session_account(&client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    trace!(state.log, "Upsert peer group: {:?}", body);

    let peer_group =
        service::upsert_peer_group(&client, &account.id, body.into_inner())
            .await?;

    debug!(state.log, "PeerGroup updated: {:?}", peer_group);

    Ok(HttpResponse::Ok().json(peer_group))
}

#[delete("/{id}")]
async fn delete_peer_group(
    state: web::Data<AppState>,
    path: Path<i32>,
    req: HttpRequest,
) -> Result<HttpResponse, Error> {
    let peer_group_id = path.into_inner();
    let client = &mut *state.pool.get().await.unwrap();
    let transaction = transaction::start(client).await?;
    let client = &DbClient::from(&transaction);

    let account = session::service::get_session_account(&client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    trace!(state.log, "Delete PeerGroup#{}", peer_group_id);

    service::delete_peer_group(&client, &account.id, &peer_group_id).await?;

    transaction.commit().await?;
    debug!(state.log, "PeerGroup#{} deleted", peer_group_id);

    Ok(HttpResponse::NoContent().finish())
}
