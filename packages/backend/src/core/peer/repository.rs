use tokio_pg_mapper::FromTokioPostgresRow;

use crate::api::error::Error;
use crate::core::peer::model::{
    Peer, PeerGroup, PeerGroupUpsertReq, PeerUpsertReq,
};
use crate::util::db_client::DbClient;
use crate::util::diff::list_diff;

pub async fn get_peer_author_id(
    client: &DbClient<'_>,
    peer_id: &i32,
) -> Result<Option<i32>, Error> {
    let stmt = client
        .prepare_cached("SELECT author_account_id FROM peer WHERE id=$1")
        .await?;
    let row = client.query_opt(&stmt, &[peer_id]).await?;
    Ok(row.map(|row| row.get(0)))
}

pub async fn get_peer_group_author_id(
    client: &DbClient<'_>,
    peer_group_id: &i32,
) -> Result<Option<i32>, Error> {
    let stmt = client
        .prepare_cached("SELECT author_account_id FROM peer_group WHERE id=$1")
        .await?;
    let row = client.query_opt(&stmt, &[peer_group_id]).await?;
    Ok(row.map(|row| row.get(0)))
}

pub async fn insert_peer(
    client: &DbClient<'_>,
    author_id: &i32,
    req: PeerUpsertReq,
) -> Result<Peer, Error> {
    let stmt = client
        .prepare_cached(
            "\
            INSERT INTO peer(author_account_id, name, display_name, email) \
            VALUES($1,$2,$2,$3) \
            RETURNING *\
            ",
        )
        .await?;
    let row = client
        .query_one(&stmt, &[author_id, &req.name, &req.email])
        .await?;
    Ok(Peer::from_row(row)?)
}

pub async fn get_peer_group_ids(
    client: &DbClient<'_>,
    peer_id: &i32,
) -> Result<Vec<i32>, Error> {
    let stmt = client
        .prepare_cached(
            "SELECT peer_group_id FROM peer_peer_group WHERE peer_id=$1",
        )
        .await?;
    let group_ids = client
        .query(&stmt, &[peer_id])
        .await?
        .iter()
        .map(|row| row.get(0))
        .collect::<Vec<i32>>();

    Ok(group_ids)
}

pub async fn set_peer_groups<'a>(
    client: &DbClient<'_>,
    peer_id: &i32,
    peer_groups: &'a Vec<i32>,
) -> Result<&'a Vec<i32>, Error> {
    client.assert_transaction().unwrap();
    let group_ids = get_peer_group_ids(client, peer_id).await?;
    let (group_ids_remove, group_ids_add) = list_diff(&group_ids, peer_groups);

    let stmt1 = client
        .prepare_cached(
            "\
            DELETE FROM peer_peer_group \
            WHERE peer_id=$1 \
                AND peer_group_id = ANY($2)\
            ",
        )
        .await?;
    client
        .execute(&stmt1, &[peer_id, &group_ids_remove])
        .await?;

    let stmt2 = &client
        .prepare_cached(
            "INSERT INTO peer_peer_group(peer_id, peer_group_id) VALUES($1,$2)",
        )
        .await?;

    let futures = group_ids_add.into_iter().map(|group_id| async move {
        client.execute(stmt2, &[peer_id, &group_id]).await
    });

    for future in futures {
        future.await.unwrap();
    }

    Ok(peer_groups)
}

pub async fn peers_by_author(
    client: &DbClient<'_>,
    author_id: &i32,
) -> Result<Vec<Peer>, Error> {
    let stmt = &client
        .prepare_cached(
            "SELECT * FROM peer WHERE author_account_id=$1 ORDER BY name ASC",
        )
        .await?;
    let peers = client
        .query(stmt, &[author_id])
        .await?
        .iter()
        .map(|row| Peer::from_row_ref(row).unwrap())
        .collect::<Vec<_>>();
    Ok(peers)
}

pub async fn update_peer(
    client: &DbClient<'_>,
    peer_id: &i32,
    req: PeerUpsertReq,
) -> Result<Peer, Error> {
    let stmt = client
        .prepare_cached(
            "UPDATE peer SET name=$2, email=$3 WHERE id=$1 RETURNING *",
        )
        .await?;
    let row = client
        .query_one(&stmt, &[peer_id, &req.name, &req.email])
        .await?;
    Ok(Peer::from_row(row)?)
}

pub async fn drop_peer(
    client: &DbClient<'_>,
    peer_id: &i32,
) -> Result<(), Error> {
    let stmt = client
        .prepare_cached("DELETE FROM peer WHERE id=$1")
        .await?;
    client.execute(&stmt, &[peer_id]).await?;
    Ok(())
}

pub async fn insert_peer_group(
    client: &DbClient<'_>,
    author_id: &i32,
    req: PeerGroupUpsertReq,
) -> Result<PeerGroup, Error> {
    let stmt = client
        .prepare_cached(
            "INSERT INTO peer_group(author_account_id, name) VALUES($1,$2) RETURNING *",
        )
        .await?;
    let row = client.query_one(&stmt, &[author_id, &req.name]).await?;
    Ok(PeerGroup::from_row(row)?)
}

pub async fn peer_groups_by_author(
    client: &DbClient<'_>,
    author_id: &i32,
) -> Result<Vec<PeerGroup>, Error> {
    let stmt = client
        .prepare_cached("SELECT * FROM peer_group WHERE author_account_id=$1")
        .await?;
    let rows = client.query(&stmt, &[author_id]).await?;
    let peer_groups = rows
        .iter()
        .map(|row| PeerGroup::from_row_ref(row).unwrap())
        .collect::<Vec<_>>();
    Ok(peer_groups)
}

pub async fn update_peer_group(
    client: &DbClient<'_>,
    peer_group_id: &i32,
    req: PeerGroupUpsertReq,
) -> Result<PeerGroup, Error> {
    let stmt = client
        .prepare_cached("UPDATE peer_group SET name=$2 WHERE id=$1 RETURNING *")
        .await?;
    let row = client.query_one(&stmt, &[peer_group_id, &req.name]).await?;
    Ok(PeerGroup::from_row(row)?)
}

pub async fn drop_peer_group(
    client: &DbClient<'_>,
    peer_group_id: &i32,
) -> Result<(), Error> {
    let stmt = client
        .prepare_cached("DELETE FROM peer_group WHERE id=$1")
        .await?;
    client.execute(&stmt, &[peer_group_id]).await?;
    Ok(())
}
