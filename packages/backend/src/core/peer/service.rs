use futures::future;

use crate::api::error::{assert_some, ClientError, Error};
use crate::core::peer::model::{
    Peer, PeerGroup, PeerGroupUpsertReq, PeerUpsertReq, PeerWithGroups,
};
use crate::core::peer::repository;
use crate::util::db_client::DbClient;

async fn create_peer(
    client: &DbClient<'_>,
    account_id: &i32,
    req: PeerUpsertReq,
) -> Result<PeerWithGroups, Error> {
    let peer = repository::insert_peer(client, account_id, req).await?;
    with_groups(client, peer).await
}

pub(super) async fn list_peers(
    client: &DbClient<'_>,
    account_id: &i32,
) -> Result<Vec<PeerWithGroups>, Error> {
    let peers = repository::peers_by_author(client, account_id).await?;
    let futures = peers.into_iter().map(|peer| with_groups(client, peer));
    future::try_join_all(futures).await
}

async fn update_peer(
    client: &DbClient<'_>,
    account_id: &i32,
    req: PeerUpsertReq,
    peer_id: &i32,
) -> Result<PeerWithGroups, Error> {
    client.assert_transaction().unwrap();
    verify_author_for_peer(client, account_id, peer_id).await?;

    repository::set_peer_groups(client, peer_id, &req.peer_groups).await?;
    let peer = repository::update_peer(client, peer_id, req).await?;
    with_groups(client, peer).await
}

pub(super) async fn upsert_peer(
    client: &DbClient<'_>,
    account_id: &i32,
    req: PeerUpsertReq,
) -> Result<PeerWithGroups, Error> {
    match req.id {
        Some(id) => update_peer(client, account_id, req, &id).await,
        None => create_peer(client, account_id, req).await,
    }
}

pub(super) async fn delete_peer(
    client: &DbClient<'_>,
    account_id: &i32,
    peer_id: &i32,
) -> Result<(), Error> {
    client.assert_transaction().unwrap();
    verify_author_for_peer(client, account_id, peer_id).await?;

    repository::drop_peer(client, peer_id).await
}

async fn with_groups(
    client: &DbClient<'_>,
    peer: Peer,
) -> Result<PeerWithGroups, Error> {
    let peer_groups = repository::get_peer_group_ids(client, &peer.id).await?;
    Ok(PeerWithGroups { peer, peer_groups })
}

async fn create_peer_group(
    client: &DbClient<'_>,
    account_id: &i32,
    req: PeerGroupUpsertReq,
) -> Result<PeerGroup, Error> {
    repository::insert_peer_group(client, account_id, req).await
}

pub(super) async fn list_peer_groups(
    client: &DbClient<'_>,
    account_id: &i32,
) -> Result<Vec<PeerGroup>, Error> {
    repository::peer_groups_by_author(client, account_id).await
}

async fn update_peer_group(
    client: &DbClient<'_>,
    account_id: &i32,
    req: PeerGroupUpsertReq,
    peer_group_id: &i32,
) -> Result<PeerGroup, Error> {
    client.assert_transaction().unwrap();
    verify_author_for_peer_group(client, account_id, peer_group_id).await?;

    repository::update_peer_group(client, peer_group_id, req).await
}

pub(super) async fn upsert_peer_group(
    client: &DbClient<'_>,
    account_id: &i32,
    req: PeerGroupUpsertReq,
) -> Result<PeerGroup, Error> {
    match req.id {
        Some(id) => update_peer_group(client, account_id, req, &id).await,
        None => create_peer_group(client, account_id, req).await,
    }
}

pub(super) async fn delete_peer_group(
    client: &DbClient<'_>,
    account_id: &i32,
    peer_group_id: &i32,
) -> Result<(), Error> {
    client.assert_transaction().unwrap();
    verify_author_for_peer_group(client, account_id, peer_group_id).await?;

    repository::drop_peer_group(client, peer_group_id).await
}

async fn verify_author_for_peer(
    client: &DbClient<'_>,
    account_id: &i32,
    peer_id: &i32,
) -> Result<(), Error> {
    let peer_author_id =
        assert_some(repository::get_peer_author_id(client, peer_id).await?)?;
    if *account_id != peer_author_id {
        Err(Error::from(ClientError::Forbidden))
    } else {
        Ok(())
    }
}

async fn verify_author_for_peer_group(
    client: &DbClient<'_>,
    account_id: &i32,
    peer_group_id: &i32,
) -> Result<(), Error> {
    let peer_group_author_id = assert_some(
        repository::get_peer_group_author_id(client, peer_group_id).await?,
    )?;
    if *account_id != peer_group_author_id {
        Err(Error::from(ClientError::Forbidden))
    } else {
        Ok(())
    }
}
