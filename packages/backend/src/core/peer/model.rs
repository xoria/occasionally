use serde_derive::{Deserialize, Serialize};
use tokio_pg_mapper_derive::PostgresMapper;
use uuid::Uuid;

#[derive(PostgresMapper, Serialize, Debug)]
#[pg_mapper(table = "peer_group")]
#[serde(rename_all = "camelCase")]
pub struct PeerGroup {
    pub id: i32,
    #[serde(skip)]
    pub author_account_id: i32,
    pub name: String,
}

#[derive(Deserialize, Debug)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct PeerGroupUpsertReq {
    pub id: Option<i32>,
    pub name: String,
}

#[derive(PostgresMapper, Serialize, Debug)]
#[pg_mapper(table = "peer")]
#[serde(rename_all = "camelCase")]
pub struct Peer {
    pub id: i32,
    #[serde(skip)]
    pub author_account_id: i32,
    pub name: String,
    pub email: Option<String>,
    pub buddy_peer_id: Option<i32>,
    pub avatar_seed: Uuid,
    // todo use enum instead
    #[serde(skip)]
    pub chat_notify_default: i32,
    pub display_name: String,
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct PeerWithGroups {
    #[serde(flatten)]
    pub peer: Peer,
    pub peer_groups: Vec<i32>,
}

#[derive(Deserialize, Debug)]
#[serde(deny_unknown_fields, rename_all = "camelCase")]
pub struct PeerUpsertReq {
    pub id: Option<i32>,
    pub name: String,
    pub email: Option<String>,
    pub peer_groups: Vec<i32>,
}
