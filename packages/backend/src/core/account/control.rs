use actix_web::{get, web, HttpResponse, Scope};

use super::service::list_all_accounts;
use crate::api::error::Error;
use crate::app_state::AppState;
use crate::util::db_client::DbClient;

pub fn routes() -> Scope {
    web::scope("/account").service(fetch_accounts)
}

#[get("")]
async fn fetch_accounts(
    state: web::Data<AppState>,
) -> Result<HttpResponse, Error> {
    let client = &*state.pool.get().await.unwrap();
    let client = &DbClient::from(client);

    let accounts = list_all_accounts(client).await?;

    Ok(HttpResponse::Ok().json(accounts))
}
