use tokio_pg_mapper::FromTokioPostgresRow;
use tokio_postgres::Error;

use super::model::{AccountBo, CreateAccountBo};
use crate::util::db_client::DbClient;

pub async fn list_all(client: &DbClient<'_>) -> Result<Vec<AccountBo>, Error> {
    let stmt = client
        .prepare_cached("SELECT * FROM account ORDER BY id DESC")
        .await?;
    let rows = client.query(&stmt, &[]).await?;
    let accounts = rows
        .iter()
        .map(|row| AccountBo::from_row_ref(row).unwrap())
        .collect::<Vec<_>>();
    Ok(accounts)
}

pub async fn by_email(
    client: &DbClient<'_>,
    email: &str,
) -> Result<Option<AccountBo>, Error> {
    let stmt = client
        .prepare_cached("SELECT * FROM account WHERE email=$1")
        .await?;
    let row = client.query_opt(&stmt, &[&email]).await?;
    Ok(row.map(|it| AccountBo::from_row(it).unwrap()))
}

pub async fn create(
    client: &DbClient<'_>,
    req: CreateAccountBo,
) -> Result<i32, Error> {
    let stmt = client
        .prepare_cached("INSERT INTO account(email) VALUES($1) RETURNING id")
        .await?;
    let row = client.query_one(&stmt, &[&req.email]).await?;
    Ok(row.get(0))
}

pub async fn by_session_token(
    client: &DbClient<'_>,
    token: &Vec<u8>,
) -> Result<Option<AccountBo>, Error> {
    let stmt = client
        .prepare_cached(
            "\
            SELECT account.* \
            FROM account \
                INNER JOIN session_token ON account.id=session_token.account_id \
            WHERE session_token.id=$1\
            ",
        )
        .await?;
    let opt_row = client.query_opt(&stmt, &[token]).await?;
    Ok(opt_row.map(|row| AccountBo::from_row(row).unwrap()))
}
