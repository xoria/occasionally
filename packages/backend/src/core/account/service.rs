use super::model::AccountBo;
use crate::api::error::Error;
use crate::core::account::model::CreateAccountBo;
use crate::core::account::repository;
use crate::util::db_client::DbClient;

pub(super) async fn list_all_accounts(
    client: &DbClient<'_>,
) -> Result<Vec<AccountBo>, Error> {
    Ok(repository::list_all(client).await?)
}

pub async fn find_account(
    client: &DbClient<'_>,
    account_email: &str,
) -> Result<Option<AccountBo>, Error> {
    Ok(repository::by_email(client, account_email).await?)
}

pub async fn create_account(
    client: &DbClient<'_>,
    req: CreateAccountBo,
) -> Result<i32, Error> {
    Ok(repository::create(client, req).await?)
}

pub async fn resolve_session_token(
    client: &DbClient<'_>,
    token: &Vec<u8>,
) -> Result<Option<AccountBo>, Error> {
    Ok(repository::by_session_token(client, token).await?)
}
