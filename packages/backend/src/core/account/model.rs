use serde_derive::Serialize;
use tokio_pg_mapper_derive::PostgresMapper;

#[derive(Serialize, PostgresMapper, Debug)]
#[pg_mapper(table = "account")]
#[serde(rename_all = "camelCase")]
pub struct AccountBo {
    #[serde(skip)]
    pub id: i32,
    pub email: String,
    pub username: Option<String>,
    #[serde(skip)]
    pub self_peer_id: Option<i32>,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct AuthorAsOutlineBo {
    #[serde(skip)]
    pub id: i32,
    pub username: Option<String>,
}

#[derive(PostgresMapper)]
#[pg_mapper(table = "account")]
pub struct CreateAccountBo {
    pub email: String,
}
