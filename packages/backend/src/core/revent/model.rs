use chrono::{DateTime, Utc};
use serde_derive::{Deserialize, Serialize};
use tokio_pg_mapper_derive::PostgresMapper;

use crate::core::account::model::AuthorAsOutlineBo;
use crate::core::revent::revent_visibility::ReventVisibility;
use crate::util::{id11_serde, id11_serde_opt};

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ReventOutline {
    #[serde(with = "id11_serde")]
    pub id: i64,
    pub author: AuthorAsOutlineBo,
    pub name: String,
}

#[derive(PostgresMapper, Serialize, Debug)]
#[pg_mapper(table = "revent")]
#[serde(rename_all = "camelCase")]
pub struct Revent {
    #[serde(with = "id11_serde")]
    pub id: i64,
    #[serde(skip)]
    pub author_account_id: i32,
    pub name: String,
    pub visibility: ReventVisibility,
    pub date_created: DateTime<Utc>,
    pub date_archive: Option<DateTime<Utc>>,
}

impl Revent {
    pub fn to_container(self: Revent) -> ReventContainer {
        ReventContainer {
            revent: self,
            root_agenda: None,
            content: None,
        }
    }
}

#[derive(PostgresMapper, Serialize, Debug)]
#[serde(rename_all = "camelCase")]
#[pg_mapper(table = "revent_agenda")]
pub struct ReventRootAgenda {
    #[serde(skip)]
    pub agenda_id: i32,
    pub date_begin: DateTime<Utc>,
    pub date_end: Option<DateTime<Utc>>,
    pub interest_min: Option<i32>,
    pub interest_max: Option<i32>,
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ReventContainer {
    #[serde(flatten)]
    pub revent: Revent,
    #[serde(flatten)]
    pub root_agenda: Option<ReventRootAgenda>,
    pub content: Option<Vec<ContentFragment>>,
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ReventFull {
    #[serde(flatten)]
    pub revent: Revent,
    #[serde(flatten)]
    pub root_agenda: ReventRootAgenda,
    pub content: Vec<ContentFragment>,
}

impl From<ReventContainer> for ReventFull {
    fn from(value: ReventContainer) -> Self {
        ReventFull {
            revent: value.revent,
            root_agenda: value.root_agenda.unwrap(),
            content: value.content.unwrap(),
        }
    }
}

#[derive(Serialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ReventPersonalOutline {
    #[serde(flatten)]
    pub revent: Revent,
    #[serde(flatten)]
    pub root_agenda: ReventRootAgenda,
}

impl From<ReventContainer> for ReventPersonalOutline {
    fn from(value: ReventContainer) -> Self {
        ReventPersonalOutline {
            revent: value.revent,
            root_agenda: value.root_agenda.unwrap(),
        }
    }
}

#[derive(PostgresMapper, Serialize, Debug)]
#[pg_mapper(table = "content_fragment")]
#[serde(rename_all = "camelCase")]
pub struct ContentFragment {
    pub id: i64,
    pub source: String,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ContentFragmentUpsertReq {
    pub id: Option<i64>,
    pub source: String,
}

#[derive(Deserialize, Debug)]
#[serde(untagged, rename_all = "camelCase")]
pub enum ContentFragmentUpsertReqItem {
    Upsert(ContentFragmentUpsertReq),
    Keep(i64),
}

#[derive(PostgresMapper)]
#[pg_mapper(table = "revent_content_fragment")]
pub struct Revent2ContentFragmentRel {
    pub revent_id: i64,
    pub content_fragment_id: i64,
    pub index: i32,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ReventRootAgendaUpsertReq {
    pub date_begin: DateTime<Utc>,
    pub date_end: Option<DateTime<Utc>>,
    pub interest_min: Option<i32>,
    pub interest_max: Option<i32>,
}

#[derive(Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ReventUpsertReq {
    #[serde(default, with = "id11_serde_opt")]
    pub id: Option<i64>,
    pub name: String,
    pub visibility: ReventVisibility,
    pub date_archive: Option<DateTime<Utc>>,
    pub content: Vec<ContentFragmentUpsertReqItem>,
    #[serde(flatten)]
    pub root_agenda: ReventRootAgendaUpsertReq,
}
