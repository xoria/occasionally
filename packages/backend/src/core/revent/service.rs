use crate::api::error::{assert_some, ClientError, Error};
use crate::core::revent::model::{
    ContentFragment, ContentFragmentUpsertReq, ContentFragmentUpsertReqItem,
    Revent2ContentFragmentRel, ReventContainer, ReventFull, ReventOutline,
    ReventPersonalOutline, ReventUpsertReq,
};
use crate::core::revent::repository;
use crate::util::db_client::DbClient;

pub(super) async fn list_public_revents(
    client: &DbClient<'_>,
) -> Result<Vec<ReventOutline>, Error> {
    Ok(repository::list_public(client).await?)
}

pub(super) async fn list_personal_revents(
    client: &DbClient<'_>,
    account_id: &i32,
) -> Result<Vec<ReventPersonalOutline>, Error> {
    let containers = repository::list_personal(client, account_id).await?;
    Ok(containers.into_iter().map(|it| it.into()).collect())
}

pub(super) async fn find_revent(
    client: &DbClient<'_>,
    account_id: &i32,
    revent_id: &i64,
) -> Result<ReventFull, Error> {
    verify_author_for_revent(client, account_id, revent_id).await?;
    let mut container = repository::by_id(client, revent_id).await?;
    container.content =
        Some(repository::content_for_id(client, revent_id).await?);
    Ok(container.into())
}

#[allow(non_snake_case)]
pub(super) async fn find_revent__outline(
    client: &DbClient<'_>,
    revent_id: &i64,
) -> Result<ReventOutline, Error> {
    Ok(repository::by_id__outline(client, revent_id).await?)
}

async fn create_revent(
    client: &DbClient<'_>,
    account_id: &i32,
    req: &ReventUpsertReq,
) -> Result<ReventContainer, Error> {
    Ok(repository::insert_revent(client, account_id, req).await?)
}

async fn update_revent(
    client: &DbClient<'_>,
    account_id: &i32,
    req: &ReventUpsertReq,
    revent_id: &i64,
) -> Result<ReventContainer, Error> {
    verify_author_for_revent(client, account_id, revent_id).await?;
    Ok(repository::update_revent(client, revent_id, req).await?)
}

pub(super) async fn upsert_revent(
    client: &DbClient<'_>,
    account_id: &i32,
    req: ReventUpsertReq,
) -> Result<ReventFull, Error> {
    client.assert_transaction().unwrap();
    let mut container = match req.id {
        Some(id) => update_revent(client, account_id, &req, &id).await,
        None => create_revent(client, account_id, &req).await,
    }?;
    container.content = Some(
        upsert_revent_content(
            client,
            account_id,
            &container.revent.id,
            req.content,
        )
        .await?,
    );
    Ok(container.into())
}

pub(super) async fn delete_revent(
    client: &DbClient<'_>,
    account_id: &i32,
    revent_id: &i64,
) -> Result<(), Error> {
    verify_author_for_revent(client, account_id, revent_id).await?;
    repository::drop_revent(client, revent_id).await?;
    Ok(())
}

async fn create_content_fragment(
    client: &DbClient<'_>,
    account_id: &i32,
    req: &ContentFragmentUpsertReq,
) -> Result<i64, Error> {
    Ok(repository::insert_content_fragment(client, account_id, req).await?)
}

async fn update_content_fragment(
    client: &DbClient<'_>,
    content_fragment_id: &i64,
    req: &ContentFragmentUpsertReq,
) -> Result<(), Error> {
    Ok(
        repository::update_content_fragment(client, content_fragment_id, req)
            .await?,
    )
}

async fn upsert_revent_content(
    client: &DbClient<'_>,
    account_id: &i32,
    revent_id: &i64,
    content: Vec<ContentFragmentUpsertReqItem>,
) -> Result<Vec<ContentFragment>, Error> {
    // precondition: author -> revent verified
    client.assert_transaction().unwrap();
    // handle create and upsert to content fragments
    let futures = content.into_iter().map(|req_item| async move {
        match req_item {
            ContentFragmentUpsertReqItem::Keep(id) => Ok(id),
            ContentFragmentUpsertReqItem::Upsert(req) => match req.id {
                Some(id) => {
                    update_content_fragment(client, &id, &req).await.map(|_| id)
                }
                None => {
                    Ok(create_content_fragment(client, account_id, &req)
                        .await?)
                }
            },
        }
    });
    let mut ids: Vec<i64> = vec![];
    for future in futures {
        let id = future.await.unwrap();
        ids.push(id);
    }
    // drop revent -> content_fragment relation
    repository::drop_content_fragment_to_revent_relation(client, revent_id)
        .await?;
    // create new revent -> content_fragment relations
    let futures = ids.into_iter().enumerate().map(|(index, id)| async move {
        repository::insert_revent_content_fragment(
            client,
            &Revent2ContentFragmentRel {
                content_fragment_id: id,
                revent_id: *revent_id,
                index: i32::try_from(index).unwrap(),
            },
        )
        .await
    });
    for future in futures {
        future.await?;
    }
    // vacuum unused content fragments
    repository::vacuum_content_fragments(client, account_id).await?;

    Ok(repository::content_for_id(client, revent_id).await?)
}

async fn verify_author_for_revent(
    client: &DbClient<'_>,
    account_id: &i32,
    revent_id: &i64,
) -> Result<(), Error> {
    let revent_author_id = assert_some(
        repository::get_revent_author_id(client, revent_id).await?,
    )?;
    if *account_id != revent_author_id {
        Err(Error::from(ClientError::Forbidden))
    } else {
        Ok(())
    }
}
