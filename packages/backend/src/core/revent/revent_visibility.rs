use std::error::Error;

use serde_derive::{Deserialize, Serialize};
use tokio_postgres::types::private::BytesMut;
use tokio_postgres::types::{to_sql_checked, FromSql, IsNull, ToSql, Type};

use crate::api::error::ServerError;

#[derive(Serialize, Deserialize, Debug)]
pub enum ReventVisibility {
    Public,
    InviteOnly,
    Link,
}

impl ReventVisibility {
    fn to_str(&self) -> &str {
        match self {
            ReventVisibility::Public => "Public",
            ReventVisibility::InviteOnly => "InviteOnly",
            ReventVisibility::Link => "Link",
        }
    }

    pub fn from_str(value: &str) -> Result<ReventVisibility, ()> {
        match value {
            "Public" => Ok(ReventVisibility::Public),
            "InviteOnly" => Ok(ReventVisibility::InviteOnly),
            "Link" => Ok(ReventVisibility::Link),
            _ => Err(()),
        }
    }
}

impl<'a> FromSql<'a> for ReventVisibility {
    fn from_sql(
        type_: &Type,
        raw: &'a [u8],
    ) -> Result<ReventVisibility, Box<dyn Error + Sync + Send>> {
        let it = <String as FromSql>::from_sql(type_, raw)?;
        // hm, `.map_err(|_| Box::new(...))` doesn't seem to work? weird.
        match ReventVisibility::from_str(it.as_str()) {
            Ok(it) => Ok(it),
            Err(_) => Err(Box::new(ServerError::FromSqlEnumError(it))),
        }
    }

    fn accepts(ty: &Type) -> bool {
        <&str as FromSql>::accepts(ty)
    }
}

impl ToSql for ReventVisibility {
    to_sql_checked!();

    fn to_sql(
        &self,
        type_: &Type,
        target: &mut BytesMut,
    ) -> Result<IsNull, Box<dyn Error + Sync + Send>> {
        <&str as ToSql>::to_sql(&self.to_str(), type_, target)
    }

    fn accepts(ty: &Type) -> bool {
        <&str as ToSql>::accepts(ty)
    }
}
