use actix_web::web::{Json, Path};
use actix_web::{delete, get, put, web, HttpRequest, HttpResponse, Scope};
use slog::{debug, trace};

use crate::api::common::{AccessQuery, AccessQueryKind};
use crate::api::error::{assert_some_or, ClientError, Error};
use crate::app_state::AppState;
use crate::core::revent::model::ReventUpsertReq;
use crate::core::revent::service;
use crate::core::session;
use crate::util::db_client::DbClient;
use crate::util::id11::to_i64;
use crate::util::transaction;

pub fn routes() -> Scope {
    web::scope("/revent")
        .service(fetch_revents)
        .service(fetch_revent)
        .service(put_revent)
        .service(delete_revent)
}

#[get("")]
async fn fetch_revents(
    state: web::Data<AppState>,
    req: HttpRequest,
    query: web::Query<AccessQuery>,
) -> Result<HttpResponse, Error> {
    match query.kind {
        Some(AccessQueryKind::Personal) => {
            fetch_personal_revents(state, req).await
        }
        _ => fetch_public_revents(state).await,
    }
}

async fn fetch_public_revents(
    state: web::Data<AppState>,
) -> Result<HttpResponse, Error> {
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let revents = service::list_public_revents(client).await?;

    Ok(HttpResponse::Ok().json(revents))
}

async fn fetch_personal_revents(
    state: web::Data<AppState>,
    req: HttpRequest,
) -> Result<HttpResponse, Error> {
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let account = session::service::get_session_account(client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    let revents = service::list_personal_revents(client, &account.id).await?;

    Ok(HttpResponse::Ok().json(revents))
}

#[get("/{id}")]
async fn fetch_revent(
    state: web::Data<AppState>,
    req: HttpRequest,
    path: Path<String>,
    query: web::Query<AccessQuery>,
) -> Result<HttpResponse, Error> {
    let revent_id = to_i64(&path.into_inner()).map_err(|_| {
        ClientError::BadRequest("Invalid Revent ID within path".to_string())
    })?;
    match query.kind {
        Some(AccessQueryKind::Personal) => {
            fetch_personal_revent(state, req, &revent_id).await
        }
        _ => fetch_public_revent(state, &revent_id).await,
    }
}

async fn fetch_public_revent(
    state: web::Data<AppState>,
    revent_id: &i64,
) -> Result<HttpResponse, Error> {
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let revent = service::find_revent__outline(client, revent_id).await?;

    Ok(HttpResponse::Ok().json(revent))
}

async fn fetch_personal_revent(
    state: web::Data<AppState>,
    req: HttpRequest,
    revent_id: &i64,
) -> Result<HttpResponse, Error> {
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let account = session::service::get_session_account(client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    let revent = service::find_revent(client, &account.id, revent_id).await?;

    Ok(HttpResponse::Ok().json(revent))
}

#[put("")]
async fn put_revent(
    state: web::Data<AppState>,
    req: HttpRequest,
    body: Json<ReventUpsertReq>,
) -> Result<HttpResponse, Error> {
    let client = &mut *state.pool.get().await.unwrap();
    let transaction = transaction::start(client).await?;
    let client = &DbClient::from(&transaction);

    let account = session::service::get_session_account(client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    trace!(state.log, "Upsert revent: {:?}", body);

    let revent =
        service::upsert_revent(&client, &account.id, body.into_inner()).await?;

    transaction.commit().await?;
    debug!(state.log, "Revent updated: {:?}", revent);

    Ok(HttpResponse::Ok().json(revent))
}

#[delete("/{id}")]
async fn delete_revent(
    state: web::Data<AppState>,
    req: HttpRequest,
    path: Path<String>,
) -> Result<HttpResponse, Error> {
    let revent_id = to_i64(&path.into_inner()).map_err(|_| {
        ClientError::BadRequest("Invalid Revent ID within path".to_string())
    })?;
    let client = state.pool.get().await.unwrap();
    let client = &DbClient::from(&*client);

    let account = session::service::get_session_account(client, &req).await?;
    let account = assert_some_or(account, ClientError::Unauthorized)?;

    trace!(state.log, "Delete revent: {}", revent_id);

    service::delete_revent(&client, &account.id, &revent_id).await?;

    debug!(state.log, "Revent deleted: {}", revent_id);

    Ok(HttpResponse::NoContent().finish())
}
