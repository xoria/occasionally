use tokio_pg_mapper::{Error, FromTokioPostgresRow};
use tokio_postgres::Row;

use crate::core::account::model::AuthorAsOutlineBo;
use crate::core::revent::model::{
    ContentFragment, ContentFragmentUpsertReq, Revent,
    Revent2ContentFragmentRel, ReventContainer, ReventOutline,
    ReventRootAgenda, ReventUpsertReq,
};
use crate::util::db_client::DbClient;

impl From<&Row> for ReventOutline {
    fn from(row: &Row) -> Self {
        Self {
            id: row.get("id"),
            author: AuthorAsOutlineBo {
                id: row.get("author_id"),
                username: row.get("author_username"),
            },
            name: row.get("name"),
        }
    }
}

impl From<&Row> for ReventContainer {
    fn from(row: &Row) -> Self {
        Self {
            revent: Revent::from_row_ref(row).unwrap(),
            root_agenda: Some(ReventRootAgenda::from_row_ref(row).unwrap()),
            content: None,
        }
    }
}

pub async fn list_public(
    client: &DbClient<'_>,
) -> Result<Vec<ReventOutline>, Error> {
    // TODO add sort order
    let stmt = client
        .prepare_cached(
            "\
            SELECT \
                revent.id, \
                revent.name, \
                author.id AS author_id, \
                author.username AS author_username \
            FROM revent \
                JOIN account author ON author.id=revent.author_account_id \
            WHERE revent.visibility='PUBLIC'\
            ",
        )
        .await?;
    let rows = client.query(&stmt, &[]).await?;
    let revents = rows
        .iter()
        .map(|row| ReventOutline::from(row))
        .collect::<Vec<_>>();
    Ok(revents)
}

pub(super) async fn list_personal(
    client: &DbClient<'_>,
    account_id: &i32,
) -> Result<Vec<ReventContainer>, Error> {
    // TODO add sort order
    let stmt = client
        .prepare_cached(
            "\
            SELECT r.*, ra.*, ra.id as agenda_id \
            FROM revent r \
                LEFT JOIN revent_agenda ra ON r.id=ra.revent_id \
            WHERE author_account_id=$1 \
                AND ra.name IS NULL\
            ",
        )
        .await?;
    let rows = client.query(&stmt, &[account_id]).await?;
    let revents = rows.iter().map(ReventContainer::from).collect::<Vec<_>>();
    Ok(revents)
}

pub(super) async fn by_id(
    client: &DbClient<'_>,
    revent_id: &i64,
) -> Result<ReventContainer, Error> {
    let stmt = client
        .prepare_cached(
            "\
            SELECT r.*, ra.*, ra.id as agenda_id \
            FROM revent r \
                LEFT JOIN revent_agenda ra ON r.id=ra.revent_id \
            WHERE r.id=$1 \
                AND ra.name IS NULL\
            ",
        )
        .await?;
    let row = client.query_one(&stmt, &[revent_id]).await?;
    Ok(ReventContainer::from(&row))
}

#[allow(non_snake_case)]
pub async fn by_id__outline(
    client: &DbClient<'_>,
    revent_id: &i64,
) -> Result<ReventOutline, Error> {
    let stmt = client
        .prepare_cached(
            "\
            SELECT \
                revent.id, \
                revent.name, \
                author.id AS author_id, \
                author.username AS author_username \
            FROM revent \
                JOIN account author ON author.id=revent.author_account_id \
            WHERE revent.id=$1\
            ",
        )
        .await?;
    let row = client.query_one(&stmt, &[revent_id]).await?;
    Ok(ReventOutline::from(&row))
}

pub async fn insert_revent(
    client: &DbClient<'_>,
    author_id: &i32,
    req: &ReventUpsertReq,
) -> Result<ReventContainer, Error> {
    client.assert_transaction().unwrap();
    let stmt_revent = client
        .prepare_cached(
            "\
            INSERT INTO revent(author_account_id, name, visibility, \
                date_archive) \
            VALUES($1,$2,$3,$4) \
            RETURNING *\
            ",
        )
        .await?;
    let row_revent = client
        .query_one(
            &stmt_revent,
            &[author_id, &req.name, &req.visibility, &req.date_archive],
        )
        .await?;
    let mut container = Revent::from_row(row_revent)?.to_container();
    let stmt_agenda = client
        .prepare_cached(
            "\
            INSERT INTO revent_agenda(revent_id, date_begin, date_end, \
                interest_min, interest_max, can_interest) \
            VALUES($1,$2,$3,$4,$5,true) \
            RETURNING id as agenda_id, *\
            ",
        )
        .await?;
    let req_agenda = &req.root_agenda;
    let row_agenda = client
        .query_one(
            &stmt_agenda,
            &[
                &container.revent.id,
                &req_agenda.date_begin,
                &req_agenda.date_end,
                &req_agenda.interest_min,
                &req_agenda.interest_max,
            ],
        )
        .await?;
    container.root_agenda = Some(ReventRootAgenda::from_row(row_agenda)?);
    Ok(container)
}

pub(crate) async fn update_revent(
    client: &DbClient<'_>,
    revent_id: &i64,
    req: &ReventUpsertReq,
) -> Result<ReventContainer, Error> {
    let stmt_revent = client
        .prepare_cached(
            "\
            UPDATE revent \
            SET name=$2, visibility=$3, date_archive=$4 \
            WHERE id=$1 \
            RETURNING *\
            ",
        )
        .await?;
    let row_revent = client
        .query_one(
            &stmt_revent,
            &[revent_id, &req.name, &req.visibility, &req.date_archive],
        )
        .await?;
    let mut container = Revent::from_row(row_revent)?.to_container();
    let stmt_agenda = client
        .prepare_cached(
            "\
            UPDATE revent_agenda \
            SET date_begin=$2, date_end=$3, interest_min=$4, interest_max=$5 \
            WHERE revent_id=$1 AND name is NULL \
            RETURNING id as agenda_id, *\
            ",
        )
        .await?;
    let req_agenda = &req.root_agenda;
    let row_agenda = client
        .query_one(
            &stmt_agenda,
            &[
                &container.revent.id,
                &req_agenda.date_begin,
                &req_agenda.date_end,
                &req_agenda.interest_min,
                &req_agenda.interest_max,
            ],
        )
        .await?;
    container.root_agenda = Some(ReventRootAgenda::from_row(row_agenda)?);
    Ok(container)
}

pub async fn get_revent_author_id(
    client: &DbClient<'_>,
    revent_id: &i64,
) -> Result<Option<i32>, Error> {
    let stmt = client
        .prepare_cached("SELECT author_account_id FROM revent WHERE id=$1")
        .await?;
    let row = client.query_opt(&stmt, &[revent_id]).await?;
    Ok(row.map(|row| row.get(0)))
}

pub async fn drop_revent(
    client: &DbClient<'_>,
    revent_id: &i64,
) -> Result<(), Error> {
    let stmt0 = client
        .prepare_cached("DELETE FROM revent WHERE id=$1")
        .await?;
    client.execute(&stmt0, &[revent_id]).await?;
    Ok(())
}

pub(super) async fn drop_content_fragment_to_revent_relation(
    client: &DbClient<'_>,
    revent_id: &i64,
) -> Result<(), Error> {
    let stmt = client
        .prepare_cached(
            "DELETE FROM revent_content_fragment WHERE revent_id=$1",
        )
        .await?;
    client.execute(&stmt, &[revent_id]).await?;
    Ok(())
}

pub(super) async fn vacuum_content_fragments(
    client: &DbClient<'_>,
    account_id: &i32,
) -> Result<u64, Error> {
    let stmt = client
        .prepare_cached(
            "\
            DELETE FROM content_fragment \
            WHERE author_account_id=$1\
                AND NOT EXISTS (\
                    SELECT 1 \
                    FROM named_content_fragment \
                    WHERE content_fragment_id=content_fragment.id\
                ) \
                AND NOT EXISTS (\
                    SELECT 1 \
                    FROM revent_content_fragment \
                    WHERE content_fragment_id=content_fragment.id\
                )\
            ",
        )
        .await?;
    Ok(client.execute(&stmt, &[account_id]).await?)
}

pub(super) async fn insert_revent_content_fragment(
    client: &DbClient<'_>,
    it: &Revent2ContentFragmentRel,
) -> Result<(), Error> {
    let stmt = client
        .prepare_cached(
            "\
            INSERT INTO revent_content_fragment(revent_id, \
                content_fragment_id, index) \
            VALUES($1,$2,$3)\
            ",
        )
        .await?;
    client
        .execute(&stmt, &[&it.revent_id, &it.content_fragment_id, &it.index])
        .await?;
    Ok(())
}

pub(super) async fn insert_content_fragment(
    client: &DbClient<'_>,
    account_id: &i32,
    req: &ContentFragmentUpsertReq,
) -> Result<i64, Error> {
    let stmt = client
        .prepare_cached(
            "\
            INSERT INTO content_fragment(author_account_id, index, source, \
                html) \
            VALUES($1,$2,$3,$4) \
            RETURNING id\
            ",
        )
        .await?;
    let row = client
        .query_one(&stmt, &[account_id, &-1, &req.source, &""])
        .await?;
    Ok(row.get(0))
}
pub(super) async fn update_content_fragment(
    client: &DbClient<'_>,
    content_fragment_id: &i64,
    req: &ContentFragmentUpsertReq,
) -> Result<(), Error> {
    let stmt = client
        .prepare_cached("UPDATE content_fragment SET source=$2 WHERE id=$1")
        .await?;
    client
        .execute(&stmt, &[content_fragment_id, &req.source])
        .await?;
    Ok(())
}

pub async fn content_for_id(
    client: &DbClient<'_>,
    revent_id: &i64,
) -> Result<Vec<ContentFragment>, Error> {
    let stmt = client
        .prepare_cached(
            "\
            SELECT content_fragment.* FROM content_fragment \
                JOIN revent_content_fragment as j ON id=j.content_fragment_id \
            WHERE j.revent_id=$1 \
            ORDER BY j.index ASC\
            ",
        )
        .await?;
    let rows = client.query(&stmt, &[revent_id]).await?;
    let fragments = rows
        .iter()
        .map(|row| ContentFragment::from_row_ref(row).unwrap())
        .collect::<Vec<_>>();
    Ok(fragments)
}
