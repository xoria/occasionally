table! {
    account (id) {
        id -> Int4,
        email -> Varchar,
        self_peer_id -> Nullable<Int4>,
        username -> Nullable<Varchar>,
    }
}

table! {
    account_role (account_id, role) {
        account_id -> Int8,
        role -> Varchar,
    }
}

table! {
    chat_message (id) {
        id -> Int8,
        date_created -> Timestamptz,
        topic_id -> Int4,
        author_peer_id -> Nullable<Int4>,
    }
}

table! {
    chat_message_blob (id) {
        id -> Int8,
        chat_message_id -> Int8,
        date_created -> Timestamptz,
        source -> Nullable<Text>,
        html -> Nullable<Text>,
    }
}

table! {
    chat_message_ping (id) {
        chat_message_id -> Int8,
        peer_id -> Int4,
        date_created -> Timestamptz,
        id -> Int8,
        transport_type -> Varchar,
        transport_id -> Nullable<Int4>,
    }
}

table! {
    chat_topic (id) {
        id -> Int4,
        revent_id -> Int8,
        index -> Int4,
        creator_peer_id -> Nullable<Int4>,
        date_created -> Timestamptz,
        date_archived -> Nullable<Timestamptz>,
        revent_agenda_id -> Nullable<Int4>,
        name -> Varchar,
    }
}

table! {
    content_fragment (id) {
        id -> Int8,
        source -> Text,
        author_account_id -> Int4,
        html -> Text,
        date_created -> Timestamptz,
        index -> Int4,
        origin_id -> Nullable<Int8>,
    }
}

table! {
    named_content_fragment (content_fragment_id) {
        content_fragment_id -> Int8,
        name -> Varchar,
        index -> Int4,
    }
}

table! {
    peer (id) {
        id -> Int4,
        name -> Varchar,
        author_account_id -> Int4,
        email -> Nullable<Varchar>,
        buddy_peer_id -> Nullable<Int4>,
        avatar_seed -> Uuid,
        chat_notify_default -> Int4,
        display_name -> Varchar,
    }
}

table! {
    peer_group (id) {
        id -> Int4,
        name -> Varchar,
        author_account_id -> Int4,
    }
}

table! {
    peer_peer_group (peer_id, peer_group_id) {
        peer_id -> Int4,
        peer_group_id -> Int4,
        state -> Varchar,
        date_update -> Timestamptz,
    }
}

table! {
    revent (id) {
        id -> Int8,
        author_account_id -> Int4,
        name -> Varchar,
        date_created -> Timestamptz,
        buddies_allowed_default -> Nullable<Int4>,
        buddies_allowed_total -> Nullable<Int4>,
        date_archive -> Nullable<Timestamptz>,
        anonymous_default -> Bool,
        has_anonymous_option -> Bool,
        visibility -> Varchar,
        allow_agenda_submissions -> Bool,
    }
}

table! {
    revent_agenda (id) {
        id -> Int4,
        date_begin -> Timestamptz,
        name -> Nullable<Varchar>,
        description -> Nullable<Varchar>,
        revent_id -> Int8,
        can_interest -> Nullable<Bool>,
        interest_min -> Nullable<Int4>,
        interest_max -> Nullable<Int4>,
        has_chat -> Bool,
        author_peer_id -> Nullable<Int4>,
        date_end -> Nullable<Timestamptz>,
        date_threshold_check -> Nullable<Timestamptz>,
        date_latest_accept -> Nullable<Timestamptz>,
    }
}

table! {
    revent_agenda_interest (revent_agenda_id, revent_invite_id) {
        revent_agenda_id -> Int4,
        revent_invite_id -> Int8,
        state -> Varchar,
        date_updated -> Timestamptz,
    }
}

table! {
    revent_content_fragment (revent_id, content_fragment_id) {
        revent_id -> Int8,
        content_fragment_id -> Int8,
        index -> Int4,
    }
}

table! {
    revent_invite (id) {
        revent_id -> Nullable<Int8>,
        peer_id -> Nullable<Int4>,
        date_updated -> Timestamptz,
        state -> Varchar,
        id -> Int8,
        buddies_allowed -> Nullable<Int4>,
        date_accepted -> Nullable<Timestamptz>,
        anonymous -> Bool,
        chat_notify -> Int4,
        date_scheduled -> Nullable<Timestamptz>,
    }
}

table! {
    revent_invite_log (id) {
        id -> Int8,
        revent_invite_id -> Int8,
        transport_id -> Nullable<Int4>,
        status -> Varchar,
        transport_type -> Nullable<Varchar>,
    }
}

table! {
    session_token__raw (id) {
        id -> Bytea,
        account_id -> Int4,
        date_created -> Timestamptz,
        date_expire -> Timestamptz,
    }
}

table! {
    temp_token_login__raw (client_rnd) {
        client_rnd -> Int8,
        token -> Bpchar,
        account_id -> Int4,
        date_created -> Timestamptz,
        date_expire -> Timestamptz,
    }
}

table! {
    temp_token_register__raw (client_rnd) {
        client_rnd -> Int8,
        token -> Bpchar,
        email -> Varchar,
        date_created -> Timestamptz,
        date_expire -> Timestamptz,
    }
}

table! {
    transport__sendmail (account_id) {
        from -> Varchar,
        account_id -> Int4,
    }
}

joinable!(account_role -> account (account_id));
joinable!(chat_message -> chat_topic (topic_id));
joinable!(chat_message -> peer (author_peer_id));
joinable!(chat_message_blob -> chat_message (id));
joinable!(chat_message_ping -> chat_message (chat_message_id));
joinable!(chat_message_ping -> peer (peer_id));
joinable!(chat_message_ping -> transport__sendmail (transport_id));
joinable!(chat_topic -> peer (creator_peer_id));
joinable!(chat_topic -> revent (revent_id));
joinable!(chat_topic -> revent_agenda (revent_agenda_id));
joinable!(content_fragment -> account (author_account_id));
joinable!(named_content_fragment -> content_fragment (content_fragment_id));
joinable!(peer_group -> account (author_account_id));
joinable!(peer_peer_group -> peer (peer_id));
joinable!(peer_peer_group -> peer_group (peer_group_id));
joinable!(revent -> account (author_account_id));
joinable!(revent_agenda -> peer (author_peer_id));
joinable!(revent_agenda -> revent (revent_id));
joinable!(revent_agenda_interest -> revent_agenda (revent_agenda_id));
joinable!(revent_agenda_interest -> revent_invite (revent_invite_id));
joinable!(revent_content_fragment -> content_fragment (content_fragment_id));
joinable!(revent_content_fragment -> revent (revent_id));
joinable!(revent_invite -> peer (peer_id));
joinable!(revent_invite -> revent (revent_id));
joinable!(revent_invite_log -> revent_invite (revent_invite_id));
joinable!(revent_invite_log -> transport__sendmail (transport_id));
joinable!(session_token__raw -> account (account_id));
joinable!(temp_token_login__raw -> account (account_id));
joinable!(transport__sendmail -> account (account_id));

allow_tables_to_appear_in_same_query!(
    account,
    account_role,
    chat_message,
    chat_message_blob,
    chat_message_ping,
    chat_topic,
    content_fragment,
    named_content_fragment,
    peer,
    peer_group,
    peer_peer_group,
    revent,
    revent_agenda,
    revent_agenda_interest,
    revent_content_fragment,
    revent_invite,
    revent_invite_log,
    session_token__raw,
    temp_token_login__raw,
    temp_token_register__raw,
    transport__sendmail,
);
