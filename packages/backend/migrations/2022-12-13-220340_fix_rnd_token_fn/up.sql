-- The old function random_token had an off-by-one error that could result in
-- less than 6 char return values.
-- In addition, we now add a length parameter and adjust the function name for
-- consistency.

create function rnd_verify_token(len int4) returns character
    language sql
as $$
    select string_agg(substr(chars, (random() * length(chars))::integer, 1), '')
        from (values ('ABCDEFGHJKLMNPQRSTUVWXYZ23456789')) as symbol(chars)
        join generate_series(1, len) on true;
$$;

alter table account_login_token
    ALTER column token set default rnd_verify_token(6);
alter table account_register_token
    ALTER column token set default rnd_verify_token(6);

drop function random_token();
