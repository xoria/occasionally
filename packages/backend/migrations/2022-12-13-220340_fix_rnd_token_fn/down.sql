-- copied function from zero_password_logins/up migration
CREATE FUNCTION random_token() RETURNS char(6) as $$
  select
    string_agg(substr(characters, (random() * length(characters) + 1)::integer, 1), '') as random_word
  from (values('ABCDEFGHJKLMNPQRSTUVWXYZ23456789')) as symbols(characters)
    -- length of word
    join generate_series(1, 6) on 1 = 1;
$$ language sql;

alter table account_login_token
    ALTER column token set default random_token();
alter table account_register_token
    ALTER column token set default random_token();

drop function rnd_verify_token(len integer);
