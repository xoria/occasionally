--
-- PostgreSQL database dump
--

--
-- Name: pg_rnd_numeric; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_rnd_numeric WITH SCHEMA public;


--
-- Name: EXTENSION pg_rnd_numeric; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pg_rnd_numeric IS 'pg_rnd_numeric:  Created by pgx';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


--
-- Name: transport_type; Type: TYPE; Schema: public; Owner: postgres
--

CREATE TYPE public.transport_type AS ENUM (
    'email'
);


ALTER TYPE public.transport_type OWNER TO postgres;

--
-- Name: account_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.account_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.account_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: account; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account (
    id integer DEFAULT nextval('public.account_id_seq'::regclass) NOT NULL,
    email character varying(1024) NOT NULL,
    username character varying(1024) NOT NULL,
    password character(42) NOT NULL,
    self_peer_id integer
);


ALTER TABLE public.account OWNER TO postgres;

--
-- Name: COLUMN account.password; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.account.password IS 'Size TBD';


--
-- Name: account_role; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.account_role (
    account_id bigint NOT NULL,
    role character varying NOT NULL
);


ALTER TABLE public.account_role OWNER TO postgres;

--
-- Name: chat_message; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chat_message (
    id bigint NOT NULL,
    date_created timestamp with time zone DEFAULT now() NOT NULL,
    topic_id integer NOT NULL,
    author_peer_id integer NOT NULL
);


ALTER TABLE public.chat_message OWNER TO postgres;

--
-- Name: chat_message_blob; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chat_message_blob (
    id bigint NOT NULL,
    chat_message_id bigint NOT NULL,
    date_created timestamp with time zone DEFAULT now() NOT NULL,
    source text,
    html text
);


ALTER TABLE public.chat_message_blob OWNER TO postgres;

--
-- Name: chat_message_blob_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chat_message_blob_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chat_message_blob_id_seq OWNER TO postgres;

--
-- Name: chat_message_blob_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chat_message_blob_id_seq OWNED BY public.chat_message_blob.id;


--
-- Name: chat_message_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chat_message_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chat_message_id_seq OWNER TO postgres;

--
-- Name: chat_message_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chat_message_id_seq OWNED BY public.chat_message.id;


--
-- Name: chat_message_ping; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chat_message_ping (
    chat_message_id bigint NOT NULL,
    peer_id integer NOT NULL,
    date_created timestamp with time zone DEFAULT now() NOT NULL,
    id bigint NOT NULL,
    transport_type character varying NOT NULL,
    transport_id integer
);


ALTER TABLE public.chat_message_ping OWNER TO postgres;

--
-- Name: chat_message_ping_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chat_message_ping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chat_message_ping_id_seq OWNER TO postgres;

--
-- Name: chat_message_ping_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chat_message_ping_id_seq OWNED BY public.chat_message_ping.id;


--
-- Name: chat_topic; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.chat_topic (
    id integer NOT NULL,
    occasion_id bigint NOT NULL,
    index integer DEFAULT 0 NOT NULL,
    creator_peer_id integer NOT NULL,
    date_created timestamp with time zone DEFAULT now() NOT NULL,
    date_archived timestamp with time zone,
    occasion_agenda_id integer,
    name character varying(1024) NOT NULL
);


ALTER TABLE public.chat_topic OWNER TO postgres;

--
-- Name: chat_topic_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.chat_topic_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.chat_topic_id_seq OWNER TO postgres;

--
-- Name: chat_topic_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.chat_topic_id_seq OWNED BY public.chat_topic.id;


--
-- Name: content_fragment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.content_fragment (
    id bigint NOT NULL,
    source text NOT NULL,
    author_account_id integer NOT NULL,
    html text NOT NULL,
    date_created timestamp with time zone DEFAULT now() NOT NULL,
    index integer NOT NULL,
    origin_id bigint
);


ALTER TABLE public.content_fragment OWNER TO postgres;

--
-- Name: TABLE content_fragment; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.content_fragment IS 'Entries are supposed to be copy-on-write.';


--
-- Name: content_fragment_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.content_fragment_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.content_fragment_id_seq OWNER TO postgres;

--
-- Name: content_fragment_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.content_fragment_id_seq OWNED BY public.content_fragment.id;


--
-- Name: named_content_fragment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.named_content_fragment (
    content_fragment_id bigint NOT NULL,
    name character varying(1024) NOT NULL,
    note character varying,
    index integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.named_content_fragment OWNER TO postgres;

--
-- Name: occasion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occasion (
    id bigint DEFAULT public.rnd_int8() NOT NULL,
    author_account_id integer NOT NULL,
    name character varying(1024) NOT NULL,
    note character varying NOT NULL,
    date_created timestamp with time zone DEFAULT now() NOT NULL,
    buddies_allowed_default integer,
    buddies_allowed_total integer,
    date_threshold_check timestamp with time zone,
    date_latest_accept timestamp with time zone,
    peers_min integer,
    peers_max integer,
    date_archive timestamp with time zone,
    anonymous_default boolean DEFAULT false NOT NULL,
    has_anonymous_option boolean DEFAULT true NOT NULL,
    visibility character varying DEFAULT 'LINK'::character varying NOT NULL,
    has_chat boolean DEFAULT true NOT NULL,
    allow_agenda_submissions boolean DEFAULT false NOT NULL
);


ALTER TABLE public.occasion OWNER TO postgres;

--
-- Name: COLUMN occasion.date_archive; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.occasion.date_archive IS 'if unset, use (latest agenda date ?? date_*) plus 2 weeks';


--
-- Name: COLUMN occasion.visibility; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.occasion.visibility IS 'PUBLIC, INVITE_ONLY, LINK';


--
-- Name: occasion_agenda; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occasion_agenda (
    id integer NOT NULL,
    date_begin timestamp with time zone NOT NULL,
    name character varying NOT NULL,
    description character varying,
    occasion_id bigint NOT NULL,
    can_interest boolean DEFAULT false,
    interest_min integer,
    interest_max integer,
    has_chat boolean DEFAULT false NOT NULL,
    author_peer_id integer NOT NULL,
    date_end timestamp with time zone
);


ALTER TABLE public.occasion_agenda OWNER TO postgres;

--
-- Name: occasion_agenda_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.occasion_agenda_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.occasion_agenda_id_seq OWNER TO postgres;

--
-- Name: occasion_agenda_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.occasion_agenda_id_seq OWNED BY public.occasion_agenda.id;


--
-- Name: occasion_agenda_interest; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occasion_agenda_interest (
    occasion_agenda_id integer NOT NULL,
    occasion_invite_id bigint NOT NULL,
    state character varying NOT NULL,
    date_updated timestamp with time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.occasion_agenda_interest OWNER TO postgres;

--
-- Name: COLUMN occasion_agenda_interest.state; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.occasion_agenda_interest.state IS 'MAYBE, ACCEPTED, REJECTED';


--
-- Name: occasion_content_fragment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occasion_content_fragment (
    occasion_id character varying NOT NULL,
    content_fragment_id bigint NOT NULL,
    index integer DEFAULT 0 NOT NULL
);


ALTER TABLE public.occasion_content_fragment OWNER TO postgres;

--
-- Name: occasion_invite; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occasion_invite (
    occasion_id bigint NOT NULL,
    peer_id integer NOT NULL,
    date_updated timestamp with time zone NOT NULL,
    state character varying DEFAULT 'CREATED'::character varying NOT NULL,
    id bigint DEFAULT public.rnd_int8() NOT NULL,
    buddies_allowed integer,
    date_accepted timestamp with time zone,
    anonymous boolean NOT NULL,
    chat_notify integer DEFAULT 2 NOT NULL,
    date_scheduled timestamp with time zone
);


ALTER TABLE public.occasion_invite OWNER TO postgres;

--
-- Name: COLUMN occasion_invite.state; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.occasion_invite.state IS 'CREATED, AWAITING_APPROVAL, INVITED, ERROR, MAYBE, ACCEPTED, REJECTED';


--
-- Name: COLUMN occasion_invite.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.occasion_invite.id IS 'used as URL token';


--
-- Name: COLUMN occasion_invite.buddies_allowed; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.occasion_invite.buddies_allowed IS 'if null, fallback to occasion#buddies_allowed_default';


--
-- Name: COLUMN occasion_invite.chat_notify; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.occasion_invite.chat_notify IS '3 = ALL, 2 = PING_ALL, 1 = PING_DIRECT, 0 = NONE';


--
-- Name: occasion_invite_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.occasion_invite_log (
    id bigint NOT NULL,
    occasion_invite_id bigint NOT NULL,
    transport_id integer,
    status character varying NOT NULL,
    note character varying,
    transport_type character varying
);


ALTER TABLE public.occasion_invite_log OWNER TO postgres;

--
-- Name: occasion_invite_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.occasion_invite_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.occasion_invite_log_id_seq OWNER TO postgres;

--
-- Name: occasion_invite_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.occasion_invite_log_id_seq OWNED BY public.occasion_invite_log.id;


--
-- Name: peer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.peer (
    id integer NOT NULL,
    name character varying(1024) NOT NULL,
    author_account_id integer NOT NULL,
    email character varying(1024),
    buddy_peer_id integer,
    avatar_seed uuid DEFAULT gen_random_uuid() NOT NULL,
    chat_notify_default integer DEFAULT 2 NOT NULL,
    author_note character varying,
    display_name character varying(1024) NOT NULL
);


ALTER TABLE public.peer OWNER TO postgres;

--
-- Name: COLUMN peer.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.peer.name IS 'managed by author, not visible to peer';


--
-- Name: COLUMN peer.chat_notify_default; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.peer.chat_notify_default IS '3 = ALL, 2 = PING_ALL, 1 = PING_DIRECT, 0 = NONE';


--
-- Name: COLUMN peer.display_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.peer.display_name IS 'managed by peer; defaults to name';


--
-- Name: peer_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.peer_group (
    id integer NOT NULL,
    name character varying NOT NULL,
    note character varying NOT NULL,
    author_account_id integer NOT NULL
);


ALTER TABLE public.peer_group OWNER TO postgres;

--
-- Name: peer_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.peer_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peer_group_id_seq OWNER TO postgres;

--
-- Name: peer_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.peer_group_id_seq OWNED BY public.peer_group.id;


--
-- Name: peer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.peer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.peer_id_seq OWNER TO postgres;

--
-- Name: peer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.peer_id_seq OWNED BY public.peer.id;


--
-- Name: peer_peer_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.peer_peer_group (
    peer_id integer NOT NULL,
    peer_group_id integer NOT NULL,
    state character varying DEFAULT 'ACTIVE'::character varying NOT NULL,
    date_update timestamp with time zone DEFAULT now() NOT NULL,
    update_note text
);


ALTER TABLE public.peer_peer_group OWNER TO postgres;

--
-- Name: COLUMN peer_peer_group.state; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.peer_peer_group.state IS 'ACTIVE, SUSPENDED_PEER, SUSPENDED_AUTHOR';


--
-- Name: transport__sendmail; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.transport__sendmail (
    "from" character varying NOT NULL,
    account_id integer NOT NULL
);


ALTER TABLE public.transport__sendmail OWNER TO postgres;

--
-- Name: chat_message id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message ALTER COLUMN id SET DEFAULT nextval('public.chat_message_id_seq'::regclass);


--
-- Name: chat_message_blob id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message_blob ALTER COLUMN id SET DEFAULT nextval('public.chat_message_blob_id_seq'::regclass);


--
-- Name: chat_message_ping id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message_ping ALTER COLUMN id SET DEFAULT nextval('public.chat_message_ping_id_seq'::regclass);


--
-- Name: chat_topic id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_topic ALTER COLUMN id SET DEFAULT nextval('public.chat_topic_id_seq'::regclass);


--
-- Name: content_fragment id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.content_fragment ALTER COLUMN id SET DEFAULT nextval('public.content_fragment_id_seq'::regclass);


--
-- Name: occasion_agenda id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_agenda ALTER COLUMN id SET DEFAULT nextval('public.occasion_agenda_id_seq'::regclass);


--
-- Name: occasion_invite_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_invite_log ALTER COLUMN id SET DEFAULT nextval('public.occasion_invite_log_id_seq'::regclass);


--
-- Name: peer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer ALTER COLUMN id SET DEFAULT nextval('public.peer_id_seq'::regclass);


--
-- Name: peer_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_group ALTER COLUMN id SET DEFAULT nextval('public.peer_group_id_seq'::regclass);


--
-- Name: account account_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_pk PRIMARY KEY (id);


--
-- Name: account_role account_role_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_role
    ADD CONSTRAINT account_role_pk PRIMARY KEY (account_id, role);


--
-- Name: chat_message_blob chat_message_blob_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message_blob
    ADD CONSTRAINT chat_message_blob_pk PRIMARY KEY (id);


--
-- Name: chat_message_ping chat_message_ping_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message_ping
    ADD CONSTRAINT chat_message_ping_pk PRIMARY KEY (id);


--
-- Name: chat_message chat_message_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message
    ADD CONSTRAINT chat_message_pk PRIMARY KEY (id);


--
-- Name: chat_topic chat_topic_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_topic
    ADD CONSTRAINT chat_topic_pk PRIMARY KEY (id);


--
-- Name: content_fragment content_fragment_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.content_fragment
    ADD CONSTRAINT content_fragment_pk PRIMARY KEY (id);


--
-- Name: named_content_fragment named_content_fragment_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.named_content_fragment
    ADD CONSTRAINT named_content_fragment_pk PRIMARY KEY (content_fragment_id);


--
-- Name: occasion_agenda_interest occasion_agenda_interest_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_agenda_interest
    ADD CONSTRAINT occasion_agenda_interest_pk PRIMARY KEY (occasion_agenda_id, occasion_invite_id);


--
-- Name: occasion_agenda occasion_agenda_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_agenda
    ADD CONSTRAINT occasion_agenda_pk PRIMARY KEY (id);


--
-- Name: occasion_content_fragment occasion_content_fragment_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_content_fragment
    ADD CONSTRAINT occasion_content_fragment_pk PRIMARY KEY (occasion_id, content_fragment_id);


--
-- Name: occasion_invite_log occasion_invite_log_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_invite_log
    ADD CONSTRAINT occasion_invite_log_pk PRIMARY KEY (id);


--
-- Name: occasion_invite occasion_invite_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_invite
    ADD CONSTRAINT occasion_invite_pk PRIMARY KEY (id);


--
-- Name: occasion_invite occasion_invite_un; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_invite
    ADD CONSTRAINT occasion_invite_un UNIQUE (occasion_id, peer_id);


--
-- Name: occasion occasion_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion
    ADD CONSTRAINT occasion_pk PRIMARY KEY (id);


--
-- Name: peer_group peer_group_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_group
    ADD CONSTRAINT peer_group_pk PRIMARY KEY (id);


--
-- Name: peer_peer_group peer_peer_group_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_peer_group
    ADD CONSTRAINT peer_peer_group_pk PRIMARY KEY (peer_id, peer_group_id);


--
-- Name: peer peer_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer
    ADD CONSTRAINT peer_pk PRIMARY KEY (id);


--
-- Name: transport__sendmail transport__email_sendmail_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport__sendmail
    ADD CONSTRAINT transport__email_sendmail_pk PRIMARY KEY (account_id);


--
-- Name: account account_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account
    ADD CONSTRAINT account_fk FOREIGN KEY (self_peer_id) REFERENCES public.peer(id);


--
-- Name: account_role account_role_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.account_role
    ADD CONSTRAINT account_role_fk FOREIGN KEY (account_id) REFERENCES public.account(id);


--
-- Name: chat_message_blob chat_message_blob_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message_blob
    ADD CONSTRAINT chat_message_blob_fk FOREIGN KEY (id) REFERENCES public.chat_message(id);


--
-- Name: chat_message chat_message_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message
    ADD CONSTRAINT chat_message_fk FOREIGN KEY (topic_id) REFERENCES public.chat_topic(id);


--
-- Name: chat_message chat_message_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message
    ADD CONSTRAINT chat_message_fk_1 FOREIGN KEY (author_peer_id) REFERENCES public.peer(id);


--
-- Name: chat_message chat_message_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message
    ADD CONSTRAINT chat_message_fk_2 FOREIGN KEY (author_peer_id) REFERENCES public.peer(id);


--
-- Name: chat_message_ping chat_message_ping_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message_ping
    ADD CONSTRAINT chat_message_ping_fk FOREIGN KEY (peer_id) REFERENCES public.peer(id);


--
-- Name: chat_message_ping chat_message_ping_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message_ping
    ADD CONSTRAINT chat_message_ping_fk_1 FOREIGN KEY (chat_message_id) REFERENCES public.chat_message(id);


--
-- Name: chat_message_ping chat_message_ping_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_message_ping
    ADD CONSTRAINT chat_message_ping_fk_2 FOREIGN KEY (transport_id) REFERENCES public.transport__sendmail(account_id);


--
-- Name: chat_topic chat_topic_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_topic
    ADD CONSTRAINT chat_topic_fk FOREIGN KEY (occasion_id) REFERENCES public.occasion(id);


--
-- Name: chat_topic chat_topic_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_topic
    ADD CONSTRAINT chat_topic_fk_1 FOREIGN KEY (creator_peer_id) REFERENCES public.peer(id);


--
-- Name: chat_topic chat_topic_fk_2; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.chat_topic
    ADD CONSTRAINT chat_topic_fk_2 FOREIGN KEY (occasion_agenda_id) REFERENCES public.occasion_agenda(id);


--
-- Name: content_fragment content_fragment_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.content_fragment
    ADD CONSTRAINT content_fragment_fk FOREIGN KEY (author_account_id) REFERENCES public.account(id);


--
-- Name: content_fragment content_fragment_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.content_fragment
    ADD CONSTRAINT content_fragment_fk_1 FOREIGN KEY (origin_id) REFERENCES public.content_fragment(id);


--
-- Name: named_content_fragment named_content_fragment_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.named_content_fragment
    ADD CONSTRAINT named_content_fragment_fk FOREIGN KEY (content_fragment_id) REFERENCES public.content_fragment(id);


--
-- Name: occasion_agenda occasion_agenda_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_agenda
    ADD CONSTRAINT occasion_agenda_fk FOREIGN KEY (occasion_id) REFERENCES public.occasion(id);


--
-- Name: occasion_agenda occasion_agenda_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_agenda
    ADD CONSTRAINT occasion_agenda_fk_1 FOREIGN KEY (author_peer_id) REFERENCES public.peer(id);


--
-- Name: occasion_agenda_interest occasion_agenda_interest_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_agenda_interest
    ADD CONSTRAINT occasion_agenda_interest_fk FOREIGN KEY (occasion_agenda_id) REFERENCES public.occasion_agenda(id);


--
-- Name: occasion_agenda_interest occasion_agenda_interest_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_agenda_interest
    ADD CONSTRAINT occasion_agenda_interest_fk_1 FOREIGN KEY (occasion_invite_id) REFERENCES public.occasion_invite(id);


--
-- Name: occasion_content_fragment occasion_content_fragment_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_content_fragment
    ADD CONSTRAINT occasion_content_fragment_fk FOREIGN KEY (content_fragment_id) REFERENCES public.occasion(id);


--
-- Name: occasion_content_fragment occasion_content_fragment_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_content_fragment
    ADD CONSTRAINT occasion_content_fragment_fk_1 FOREIGN KEY (content_fragment_id) REFERENCES public.content_fragment(id);


--
-- Name: occasion occasion_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion
    ADD CONSTRAINT occasion_fk FOREIGN KEY (author_account_id) REFERENCES public.account(id);


--
-- Name: occasion_invite occasion_invite_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_invite
    ADD CONSTRAINT occasion_invite_fk FOREIGN KEY (occasion_id) REFERENCES public.occasion(id);


--
-- Name: occasion_invite occasion_invite_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_invite
    ADD CONSTRAINT occasion_invite_fk_1 FOREIGN KEY (peer_id) REFERENCES public.peer(id);


--
-- Name: occasion_invite_log occasion_invite_log_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_invite_log
    ADD CONSTRAINT occasion_invite_log_fk FOREIGN KEY (occasion_invite_id) REFERENCES public.occasion_invite(id);


--
-- Name: occasion_invite_log occasion_invite_log_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.occasion_invite_log
    ADD CONSTRAINT occasion_invite_log_fk_1 FOREIGN KEY (transport_id) REFERENCES public.transport__sendmail(account_id);


--
-- Name: peer peer_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer
    ADD CONSTRAINT peer_fk FOREIGN KEY (author_account_id) REFERENCES public.account(id);


--
-- Name: peer peer_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer
    ADD CONSTRAINT peer_fk_1 FOREIGN KEY (buddy_peer_id) REFERENCES public.peer(id);


--
-- Name: peer_group peer_group_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_group
    ADD CONSTRAINT peer_group_fk FOREIGN KEY (author_account_id) REFERENCES public.account(id);


--
-- Name: peer_peer_group peer_peer_group_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_peer_group
    ADD CONSTRAINT peer_peer_group_fk FOREIGN KEY (peer_id) REFERENCES public.peer(id);


--
-- Name: peer_peer_group peer_peer_group_fk_1; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.peer_peer_group
    ADD CONSTRAINT peer_peer_group_fk_1 FOREIGN KEY (peer_group_id) REFERENCES public.peer_group(id);


--
-- Name: transport__sendmail transport__email_sendmail_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.transport__sendmail
    ADD CONSTRAINT transport__email_sendmail_fk FOREIGN KEY (account_id) REFERENCES public.account(id);


--
-- PostgreSQL database dump complete
--

