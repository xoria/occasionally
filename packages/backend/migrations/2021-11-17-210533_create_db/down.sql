DROP EXTENSION IF EXISTS pg_rnd_numeric;
DROP EXTENSION IF EXISTS pgcrypto;
DROP TYPE IF EXISTS public.transport_type AS ENUM;
DROP SEQUENCE IF EXISTS public.account_id_seq;
DROP TABLE IF EXISTS public.account;
DROP TABLE IF EXISTS public.account_role;
DROP TABLE IF EXISTS public.chat_message;
DROP TABLE IF EXISTS public.chat_message_blob;
DROP SEQUENCE IF EXISTS public.chat_message_blob_id_seq
DROP SEQUENCE IF EXISTS public.chat_message_id_seq
DROP TABLE IF EXISTS public.chat_message_ping;
DROP SEQUENCE IF EXISTS public.chat_message_ping_id_seq
DROP TABLE IF EXISTS public.chat_topic;
DROP SEQUENCE IF EXISTS public.chat_topic_id_seq
DROP TABLE IF EXISTS public.content_fragment;
DROP SEQUENCE IF EXISTS public.content_fragment_id_seq
DROP TABLE IF EXISTS public.named_content_fragment;
DROP TABLE IF EXISTS public.occasion;
DROP TABLE IF EXISTS public.occasion_agenda;
DROP SEQUENCE IF EXISTS public.occasion_agenda_id_seq
DROP TABLE IF EXISTS public.occasion_agenda_interest;
DROP TABLE IF EXISTS public.occasion_content_fragment;
DROP TABLE IF EXISTS public.occasion_invite;
DROP TABLE IF EXISTS public.occasion_invite_log;
DROP SEQUENCE IF EXISTS public.occasion_invite_log_id_seq
DROP TABLE IF EXISTS public.peer;
DROP TABLE IF EXISTS public.peer_group;
DROP SEQUENCE IF EXISTS public.peer_group_id_seq
DROP SEQUENCE IF EXISTS public.peer_id_seq
DROP TABLE IF EXISTS public.peer_peer_group;
DROP TABLE IF EXISTS public.transport__sendmail;
