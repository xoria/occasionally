alter table revent drop column note;
alter table peer_peer_group drop column update_note;
alter table peer drop column author_note;
alter table named_content_fragment drop column note;
alter table revent_invite_log drop column note;
