alter table revent add column note varchar;
alter table peer_peer_group add column update_note varchar;
alter table peer add column author_note varchar;
alter table named_content_fragment add column note varchar;
alter table revent_invite_log add column note varchar;
