alter table revent_agenda alter column name set not null;
comment on table revent_agenda is NULL;
comment on column revent_agenda.name is NULL;
drop index revent_agenda_uk;

alter table revent add column peers_min integer, add column peers_max integer, add column date_threshold_check timestamp with time zone, add column date_latest_accept timestamp with time zone, add column has_chat boolean default true not null;
alter table revent_agenda drop column date_threshold_check, drop column date_latest_accept;

