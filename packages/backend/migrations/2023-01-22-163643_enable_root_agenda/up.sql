alter table revent_agenda alter column name drop not null;
comment on table revent_agenda is 'Every revent must have exactly one root agenda (revent_agenda.name is NULL). It may have additional non-root agenda entries.';
comment on column revent_agenda.name is 'root agenda iff null';
create unique index revent_agenda_uk on revent_agenda(revent_id) where revent_agenda.name is null;

alter table revent drop column peers_max, drop column peers_min, drop column date_threshold_check, drop column date_latest_accept, drop column has_chat;
alter table revent_agenda add column date_threshold_check timestamp with time zone, add column date_latest_accept timestamp with time zone;
