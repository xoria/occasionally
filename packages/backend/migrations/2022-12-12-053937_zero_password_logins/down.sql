alter table public.account add password character(42) not null;

drop table public.account_register_token;
drop table public.account_login_token;

drop function random_token() restrict;
