alter table public.account drop password;

CREATE OR REPLACE FUNCTION random_token() RETURNS char(6) as $$
  select
    string_agg(substr(characters, (random() * length(characters) + 1)::integer, 1), '') as random_word
  from (values('ABCDEFGHJKLMNPQRSTUVWXYZ23456789')) as symbols(characters)
    -- length of word
    join generate_series(1, 6) on 1 = 1;
$$ language sql;

create table public.account_register_token (
  client_rnd bigint primary key default public.rnd_int8() not null,
  token char(6) default random_token() not null,
  email character varying(1024) not null
);

create table public.account_login_token (
  client_rnd bigint primary key default public.rnd_int8(),
  token char(6) default random_token() not null,
  account_id integer not null,
  constraint fk_account
    foreign key (account_id)
    references public.account(id)
);
