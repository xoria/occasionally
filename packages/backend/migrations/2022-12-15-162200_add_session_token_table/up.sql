create table session_token__raw
(
    id           bytea                             default rnd_bytea(16) primary key not null,
    account_id   int4                     not null references account,
    date_created timestamp with time zone not null default now(),
    date_expire  timestamp with time zone not null
);

create view session_token as
select *
from session_token__raw
where date_expire > now();
