alter table chat_message drop constraint chat_message_fk_2;

alter table only public.chat_message alter column author_peer_id drop not null;
alter table only public.chat_topic alter column creator_peer_id drop not null;
alter table only public.revent_agenda alter column author_peer_id drop not null;
alter table only public.revent_invite alter column revent_id drop not null;
alter table only public.revent_invite alter column peer_id drop not null;

ALTER TABLE ONLY public.account drop constraint account_fk,   ADD CONSTRAINT account_fk FOREIGN KEY (self_peer_id) REFERENCES public.peer(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE ONLY public.chat_message drop constraint chat_message_fk_1,   ADD CONSTRAINT chat_message_fk_1 FOREIGN KEY (author_peer_id) REFERENCES public.peer(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE ONLY public.chat_message_ping drop constraint chat_message_ping_fk_2,   ADD CONSTRAINT chat_message_ping_fk_2 FOREIGN KEY (transport_id) REFERENCES public.transport__sendmail(account_id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE ONLY public.chat_topic drop constraint chat_topic_fk_1,   ADD CONSTRAINT chat_topic_fk_1 FOREIGN KEY (creator_peer_id) REFERENCES public.peer(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE ONLY public.content_fragment drop constraint content_fragment_fk_1,   ADD CONSTRAINT content_fragment_fk_1 FOREIGN KEY (origin_id) REFERENCES public.content_fragment(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE ONLY public.peer drop constraint peer_fk_1,   ADD CONSTRAINT peer_fk_1 FOREIGN KEY (buddy_peer_id) REFERENCES public.peer(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_agenda drop constraint revent_agenda_fk_1,   ADD CONSTRAINT revent_agenda_fk_1 FOREIGN KEY (author_peer_id) REFERENCES public.peer(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_invite drop constraint revent_invite_fk,   ADD CONSTRAINT revent_invite_fk FOREIGN KEY (revent_id) REFERENCES public.revent(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_invite drop constraint revent_invite_fk_1,   ADD CONSTRAINT revent_invite_fk_1 FOREIGN KEY (peer_id) REFERENCES public.peer(id) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_invite_log drop constraint revent_invite_log_fk_1,   ADD CONSTRAINT revent_invite_log_fk_1 FOREIGN KEY (transport_id) REFERENCES public.transport__sendmail(account_id) ON DELETE SET NULL ON UPDATE CASCADE;


ALTER TABLE ONLY public.account_role drop constraint account_role_fk,   ADD CONSTRAINT account_role_fk FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.chat_message_blob drop constraint chat_message_blob_fk,   ADD CONSTRAINT chat_message_blob_fk FOREIGN KEY (id) REFERENCES public.chat_message(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.chat_message drop constraint chat_message_fk,   ADD CONSTRAINT chat_message_fk FOREIGN KEY (topic_id) REFERENCES public.chat_topic(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.chat_message_ping drop constraint chat_message_ping_fk,   ADD CONSTRAINT chat_message_ping_fk FOREIGN KEY (peer_id) REFERENCES public.peer(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.chat_message_ping drop constraint chat_message_ping_fk_1,   ADD CONSTRAINT chat_message_ping_fk_1 FOREIGN KEY (chat_message_id) REFERENCES public.chat_message(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.chat_topic drop constraint chat_topic_fk,   ADD CONSTRAINT chat_topic_fk FOREIGN KEY (revent_id) REFERENCES public.revent(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.chat_topic drop constraint chat_topic_fk_2,   ADD CONSTRAINT chat_topic_fk_2 FOREIGN KEY (revent_agenda_id) REFERENCES public.revent_agenda(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.content_fragment drop constraint content_fragment_fk,   ADD CONSTRAINT content_fragment_fk FOREIGN KEY (author_account_id) REFERENCES public.account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.temp_token_login__raw drop constraint fk_account,   ADD CONSTRAINT fk_account FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.named_content_fragment drop constraint named_content_fragment_fk,   ADD CONSTRAINT named_content_fragment_fk FOREIGN KEY (content_fragment_id) REFERENCES public.content_fragment(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.peer drop constraint peer_fk,   ADD CONSTRAINT peer_fk FOREIGN KEY (author_account_id) REFERENCES public.account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.peer_group drop constraint peer_group_fk,   ADD CONSTRAINT peer_group_fk FOREIGN KEY (author_account_id) REFERENCES public.account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.peer_peer_group drop constraint peer_peer_group_fk,   ADD CONSTRAINT peer_peer_group_fk FOREIGN KEY (peer_id) REFERENCES public.peer(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.peer_peer_group drop constraint peer_peer_group_fk_1,   ADD CONSTRAINT peer_peer_group_fk_1 FOREIGN KEY (peer_group_id) REFERENCES public.peer_group(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_agenda drop constraint revent_agenda_fk,   ADD CONSTRAINT revent_agenda_fk FOREIGN KEY (revent_id) REFERENCES public.revent(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_agenda_interest drop constraint revent_agenda_interest_fk,   ADD CONSTRAINT revent_agenda_interest_fk FOREIGN KEY (revent_agenda_id) REFERENCES public.revent_agenda(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_agenda_interest drop constraint revent_agenda_interest_fk_1,   ADD CONSTRAINT revent_agenda_interest_fk_1 FOREIGN KEY (revent_invite_id) REFERENCES public.revent_invite(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_content_fragment drop constraint revent_content_fragment_fk,   ADD CONSTRAINT revent_content_fragment_fk FOREIGN KEY (revent_id) REFERENCES public.revent(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_content_fragment drop constraint revent_content_fragment_fk_1,   ADD CONSTRAINT revent_content_fragment_fk_1 FOREIGN KEY (content_fragment_id) REFERENCES public.content_fragment(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent drop constraint revent_fk,   ADD CONSTRAINT revent_fk FOREIGN KEY (author_account_id) REFERENCES public.account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.revent_invite_log drop constraint revent_invite_log_fk,   ADD CONSTRAINT revent_invite_log_fk FOREIGN KEY (revent_invite_id) REFERENCES public.revent_invite(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.session_token__raw drop constraint session_token__raw_account_id_fkey,   ADD CONSTRAINT session_token__raw_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE ONLY public.transport__sendmail drop constraint transport__sendmail_fk,   ADD CONSTRAINT transport__sendmail_fk FOREIGN KEY (account_id) REFERENCES public.account(id) ON DELETE CASCADE ON UPDATE CASCADE;
