ALTER TABLE public.account_login_token ADD date_created timestamp with time zone DEFAULT now() NOT NULL;
ALTER TABLE public.account_register_token ADD date_created timestamp with time zone DEFAULT now() NOT NULL;
