alter sequence revent_agenda_id_seq rename to occasion_agenda_id_seq;
alter sequence revent_invite_log_id_seq rename to occasion_invite_log_id_seq;

alter index revent_agenda_pk rename to occasion_agenda_pk;
alter index revent_agenda_interest_pk rename to occasion_agenda_interest_pk;
alter index revent_invite_log_pk rename to occasion_invite_log_pk;
alter index revent_invite_pk rename to occasion_invite_pk;
alter index revent_pk rename to occasion_pk;

alter table chat_topic rename column revent_id to occasion_id;
alter table chat_topic rename column revent_agenda_id to occasion_agenda_id;
alter table revent_agenda rename column revent_id to occasion_id;
alter table revent_agenda_interest rename column revent_agenda_id to occasion_agenda_id;
alter table revent_agenda_interest rename column revent_invite_id to occasion_invite_id;
alter table revent_content_fragment rename column revent_id to occasion_id;
alter table revent_invite rename column revent_id to occasion_id;
alter table revent_invite_log rename column revent_invite_id to occasion_invite_id;

alter table revent rename constraint revent_fk to occasion_fk;
alter table revent_agenda rename constraint revent_agenda_fk to occasion_agenda_fk;
alter table revent_agenda rename constraint revent_agenda_fk_1 to occasion_agenda_fk_1;
alter table revent_agenda_interest rename constraint revent_agenda_interest_fk to occasion_agenda_interest_fk;
alter table revent_agenda_interest rename constraint revent_agenda_interest_fk_1 to occasion_agenda_interest_fk_1;
alter table revent_content_fragment rename constraint revent_content_fragment_fk to occasion_content_fragment_fk;
alter table revent_content_fragment rename constraint revent_content_fragment_fk_1 to occasion_content_fragment_fk_1;
alter table revent_invite rename constraint revent_invite_fk to occasion_invite_fk;
alter table revent_invite rename constraint revent_invite_fk_1 to occasion_invite_fk_1;
alter table revent_invite_log rename constraint revent_invite_log_fk to occasion_invite_log_fk;
alter table revent_invite_log rename constraint revent_invite_log_fk_1 to occasion_invite_log_fk_1;

alter table transport__sendmail rename constraint transport__sendmail_fk to transport__email_sendmail_fk;
alter table transport__sendmail rename constraint transport__sendmail_pk to transport__email_sendmail_pk;

alter table revent rename to occasion;
alter table revent_agenda rename to occasion_agenda;
alter table revent_agenda_interest rename to occasion_agenda_interest;
alter table revent_content_fragment rename to occasion_content_fragment;
alter table revent_invite rename to occasion_invite;
alter table revent_invite_log rename to occasion_invite_log;
