alter table occasion rename to revent;
alter table occasion_agenda rename to revent_agenda;
alter table occasion_agenda_interest rename to revent_agenda_interest;
alter table occasion_content_fragment rename to revent_content_fragment;
alter table occasion_invite rename to revent_invite;
alter table occasion_invite_log rename to revent_invite_log;

alter sequence occasion_agenda_id_seq rename to revent_agenda_id_seq;
alter sequence occasion_invite_log_id_seq rename to revent_invite_log_id_seq;

alter index occasion_agenda_pk rename to revent_agenda_pk;
alter index occasion_agenda_interest_pk rename to revent_agenda_interest_pk;
alter index occasion_invite_log_pk rename to revent_invite_log_pk;
alter index occasion_invite_pk rename to revent_invite_pk;
alter index occasion_pk rename to revent_pk;

alter table chat_topic rename column occasion_id to revent_id;
alter table chat_topic rename column occasion_agenda_id to revent_agenda_id;
alter table revent_agenda rename column occasion_id to revent_id;
alter table revent_agenda_interest rename column occasion_agenda_id to revent_agenda_id;
alter table revent_agenda_interest rename column occasion_invite_id to revent_invite_id;
alter table revent_content_fragment rename column occasion_id to revent_id;
alter table revent_invite rename column occasion_id to revent_id;
alter table revent_invite_log rename column occasion_invite_id to revent_invite_id;

alter table revent rename constraint occasion_fk to revent_fk;
alter table revent_agenda rename constraint occasion_agenda_fk to revent_agenda_fk;
alter table revent_agenda rename constraint occasion_agenda_fk_1 to revent_agenda_fk_1;
alter table revent_agenda_interest rename constraint occasion_agenda_interest_fk to revent_agenda_interest_fk;
alter table revent_agenda_interest rename constraint occasion_agenda_interest_fk_1 to revent_agenda_interest_fk_1;
alter table revent_content_fragment rename constraint occasion_content_fragment_fk to revent_content_fragment_fk;
alter table revent_content_fragment rename constraint occasion_content_fragment_fk_1 to revent_content_fragment_fk_1;
alter table revent_invite rename constraint occasion_invite_fk to revent_invite_fk;
alter table revent_invite rename constraint occasion_invite_fk_1 to revent_invite_fk_1;
alter table revent_invite_log rename constraint occasion_invite_log_fk to revent_invite_log_fk;
alter table revent_invite_log rename constraint occasion_invite_log_fk_1 to revent_invite_log_fk_1;

-- just some tidy-up I noticed...
alter table transport__sendmail rename constraint transport__email_sendmail_fk to transport__sendmail_fk;
alter table transport__sendmail rename constraint transport__email_sendmail_pk to transport__sendmail_pk;
