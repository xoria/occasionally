alter table account_login_token rename to temp_token_login__raw;
alter index account_login_token_pkey rename to temp_token_login__raw_pkey;
alter table temp_token_login__raw add column date_expire timestamp with time zone default now() + interval '1h' not null;

alter table account_register_token rename to temp_token_register__raw;
alter index account_register_token_pkey rename to temp_token_register__raw_pkey;
alter table temp_token_register__raw add column date_expire timestamp with time zone default now() + interval '1h' not null;

create view temp_token_register as
select * from temp_token_register__raw where date_expire > now();

create view temp_token_login as
select * from temp_token_login__raw where date_expire > now();
