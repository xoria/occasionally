drop view temp_token_login;
drop view temp_token_register;

alter table temp_token_register__raw drop column date_expire;
alter index temp_token_register__raw_pkey rename to account_register_token_pkey;
alter table temp_token_register__raw rename to account_register_token;

alter table temp_token_login__raw drop column date_expire;
alter index temp_token_login__raw_pkey rename to account_login_token_pkey;
alter table temp_token_login__raw rename to account_login_token;
